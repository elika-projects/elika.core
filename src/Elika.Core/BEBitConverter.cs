﻿using System;
using System.Runtime.CompilerServices;

namespace Elika
{
    /// <summary>
    ///     Конвертер работы с Big Endian
    /// </summary>
    public static class BEBitConverter
    {
        #region Int32

        /// <summary>
        ///     Получить байты из значения
        /// </summary>
        /// <param name="value">Значение</param>
        /// <returns>Массив байтов</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] GetBytes(int value)
        {
            var buff = new byte[4];
            GetBytes(value, buff);
            return buff;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(int value, Memory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer.Span[offset + 0] = (byte)(value >> 24);
            buffer.Span[offset + 1] = (byte)(value >> 16);
            buffer.Span[offset + 2] = (byte)(value >> 8);
            buffer.Span[offset + 3] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(int value, ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 4)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(int value, byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)value;
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static int ToInt32(ReadOnlyMemory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return buffer.Span[offset + 0] << 24 |
                    buffer.Span[offset + 1] << 16 |
                    buffer.Span[offset + 2] << 8 |
                    buffer.Span[offset + 3];
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static int ToInt32(ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 4)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return buffer[offset + 0] << 24 |
                   buffer[offset + 1] << 16 |
                   buffer[offset + 2] << 8 |
                   buffer[offset + 3];
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static int ToInt32(byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return buffer[offset + 0] << 24 |
                   buffer[offset + 1] << 16 |
                   buffer[offset + 2] << 8 |
                   buffer[offset + 3];
        }

        #endregion

        #region UInt32

        /// <summary>
        ///     Получить байты из значения
        /// </summary>
        /// <param name="value">Значение</param>
        /// <returns>Массив байтов</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] GetBytes(uint value)
        {
            var buff = new byte[4];
            GetBytes(value, buff);
            return buff;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(uint value, Memory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer.Span[offset + 0] = (byte)(value >> 24);
            buffer.Span[offset + 1] = (byte)(value >> 16);
            buffer.Span[offset + 2] = (byte)(value >> 8);
            buffer.Span[offset + 3] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(uint value, ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 4)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(uint value, byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)value;
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static uint ToUInt32(ReadOnlyMemory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (uint)(buffer.Span[offset + 0] << 24 |
                            buffer.Span[offset + 1] << 16 |
                            buffer.Span[offset + 2] << 8 |
                            buffer.Span[offset + 3]);
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static uint ToUInt32(ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 4)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (uint)(buffer[offset + 0] << 24 |
                          buffer[offset + 1] << 16 |
                          buffer[offset + 2] << 8 |
                          buffer[offset + 3]);
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static uint ToUInt32(byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 4)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (uint)(buffer[offset + 0] << 24 |
                          buffer[offset + 1] << 16 |
                          buffer[offset + 2] << 8 |
                          buffer[offset + 3]);
        }

        #endregion

        #region Int16

        /// <summary>
        ///     Получить байты из значения
        /// </summary>
        /// <param name="value">Значение</param>
        /// <returns>Массив байтов</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] GetBytes(short value)
        {
            var buff = new byte[2];
            GetBytes(value, buff);
            return buff;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(short value, Memory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer.Span[offset + 0] = (byte)(value >> 8);
            buffer.Span[offset + 1] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(short value, ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 2)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 8);
            buffer[offset + 1] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(short value, byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 8);
            buffer[offset + 1] = (byte)value;
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static short ToInt16(ReadOnlyMemory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (short)(buffer.Span[offset + 0] << 8 |
                            buffer.Span[offset + 1]);
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static short ToInt16(ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 2)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (short)(buffer[offset + 0] << 8 |
                           buffer[offset + 1]);
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static short ToInt16(byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (short)(buffer[offset + 0] << 8 |
                           buffer[offset + 1]);
        }

        #endregion

        #region UInt16

        /// <summary>
        ///     Получить байты из значения
        /// </summary>
        /// <param name="value">Значение</param>
        /// <returns>Массив байтов</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] GetBytes(ushort value)
        {
            var buff = new byte[2];
            GetBytes(value, buff);
            return buff;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(ushort value, Memory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer.Span[offset + 0] = (byte)(value >> 8);
            buffer.Span[offset + 1] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(ushort value, ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 2)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 8);
            buffer[offset + 1] = (byte)value;
        }

        /// <summary>
        ///     Записать байты из значения в буфер
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="buffer">Буфер</param>
        /// <param name="offset">Смещение для начала записи</param>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static void GetBytes(ushort value, byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для записи в буфер");

            buffer[offset + 0] = (byte)(value >> 8);
            buffer[offset + 1] = (byte)value;
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static ushort ToUInt16(ReadOnlyMemory<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (ushort)(buffer.Span[offset + 0] << 8 |
                            buffer.Span[offset + 1]);
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static ushort ToUInt16(ArraySegment<byte> buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Count - offset < 2)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (ushort)(buffer[offset + 0] << 8 |
                            buffer[offset + 1]);
        }

        /// <summary>
        ///     Получить значение из массива байт
        /// </summary>
        /// <param name="buffer">Массив байт</param>
        /// <param name="offset">Смещение для начала чтения</param>
        /// <returns>Значение</returns>
        /// <exception cref="ArgumentException">Не хватает места</exception>
        public static ushort ToUInt16(byte[] buffer, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(buffer);

            if (buffer.Length - offset < 2)
                throw new ArgumentException("Недостаточно места для чтения из буфера");

            return (ushort)(buffer[offset + 0] << 8 |
                            buffer[offset + 1]);
        }

        #endregion
    }
}
