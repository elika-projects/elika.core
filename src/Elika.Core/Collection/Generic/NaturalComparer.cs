﻿using System;
using System.Collections.Generic;

namespace Elika.Collection.Generic;

/// <summary>
///     Компаратор для натуальной сортировки строк
/// </summary>
public class NaturalComparer : IComparer<string>
{
    /// <inheritdoc />
    public int Compare(string x, string y)
    {
        var xGroups = SplitSymbols(x);
        var yGroups = SplitSymbols(y);
        var minCount = Math.Min(xGroups.Count, yGroups.Count);
        for (var i = 0; i < minCount; i++)
            if (int.TryParse(xGroups[i], out var xNumber))
            {
                if (int.TryParse(yGroups[i], out var yNumber))
                {
                    var res = xNumber.CompareTo(yNumber);
                    if (res != 0)
                        return res;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                var res = string.Compare(xGroups[i], yGroups[i], StringComparison.Ordinal);
                if (res != 0)
                    return res;
            }

        return string.Compare(x, y, StringComparison.Ordinal);
    }

    private static List<string> SplitSymbols(string str)
    {
        var result = new List<string>();
        var numbers = string.Empty;
        var characters = string.Empty;
        foreach (var character in str ?? string.Empty)
        {
            if (char.IsDigit(character))
                numbers += character;
            else
                characters += character;

            if (string.IsNullOrEmpty(numbers) || string.IsNullOrEmpty(characters)) continue;

            if (!char.IsDigit(character))
            {
                result.Add(numbers);
                numbers = string.Empty;
            }
            else
            {
                result.Add(characters);
                characters = string.Empty;
            }
        }

        if (!string.IsNullOrEmpty(numbers)) result.Add(numbers);
        if (!string.IsNullOrEmpty(characters)) result.Add(characters);
        return result;
    }
}