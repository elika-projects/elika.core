﻿using Elika.Reflection;
using System;
using System.Diagnostics;

namespace Elika.Diagnostics
{
    /// <summary>
    ///     Расширения для <see cref="ActivitySource"/>
    /// </summary>
    public static class ActivitySourceExtensions
    {
        /// <summary>
        ///     Создать новую активность есть активные слушатели, используя в качестви имени вызывающего наименование типа и метода, а так же тип активности
        /// </summary>
        /// <param name="source">Источник</param>
        /// <param name="kind">Тип активности</param>
        /// <returns>Активность</returns>
        public static Activity? StartActivityWithFullName(this ActivitySource? source, ActivityKind kind = ActivityKind.Internal)
        {
            var frame = Caller.GetCallerStackFrame(1);

            if (frame is null)
                return null;

            var methodInfo = frame?.GetMethod();

            if (methodInfo?.DeclaringType is null)
                return null;

            var activity = source.StartActivityInternal(methodInfo.DeclaringType, methodInfo.Name, kind);
            activity?.AddBaggage("Source File Name", frame?.GetFileName() ?? "NONE");
            activity?.AddBaggage("Source Line", frame?.GetFileLineNumber().ToString() ?? "-1");
            return activity;
        }

        private static Activity? StartActivityInternal(this ActivitySource? source, Type objType, string memberName, ActivityKind kind)
        {
            if (source is null)
                return null;

            return source.StartActivity($"{objType.Name}.{memberName}", kind);
        }
    }
}
