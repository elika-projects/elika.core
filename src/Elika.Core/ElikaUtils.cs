﻿using Elika.Exceptions;
using System.Linq;
using System.Net.NetworkInformation;

namespace Elika
{
    /// <summary>
    ///     Вспомогательные утилиты
    /// </summary>
    public class ElikaUtils
    {
        /// <summary>
        ///     Получить свободный порт
        /// </summary>
        /// <param name="min">Минимальный номер порта</param>
        /// <param name="max">Максимальный номер порта</param>
        /// <returns>Свободный порт</returns>
        public static int GetFreePort(int min = 1, int max = ushort.MaxValue)
        {
            var global = IPGlobalProperties.GetIPGlobalProperties();
            var busyPorts = global.GetActiveTcpConnections()
                .Select(c => c.LocalEndPoint.Port)
                .Concat(global.GetActiveTcpListeners().Select(c => c.Port))
                .Concat(global.GetActiveUdpListeners().Select(c => c.Port))
                .ToHashSet();
            
            for(var port = min; port <= max; port++)
                if(!busyPorts.Contains(port))
                    return port;

            throw new NotFreePortException();
        }
    }
}
