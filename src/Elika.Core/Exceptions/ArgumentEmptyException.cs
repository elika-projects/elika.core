﻿using System;

namespace Elika.Exceptions;

/// <summary>
///     Исключение, которое создаётся когда аргумент пустой (<see cref="string.Empty" />)
/// </summary>
public class ArgumentEmptyException : ArgumentException, IElikaException
{
    private static readonly string _msg = "Значение не может быть пустым";

    /// <summary>
    ///     Инициализурует исключение типа <see cref="ArgumentEmptyException" />
    /// </summary>
    public ArgumentEmptyException() : base(_msg)
    {
    }

    /// <summary>
    ///     Инициализурует исключение типа <see cref="ArgumentEmptyException" /> c указанием наименования аргумента
    /// </summary>
    /// <param name="paramName">Наименование аргумента</param>
    public ArgumentEmptyException(string paramName) : base(_msg, paramName)
    {
    }
}