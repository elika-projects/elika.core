﻿using System;

namespace Elika.Exceptions;

/// <summary>
///     Исключение, которое создаётся когда аргумент не является атрибутом
/// </summary>
internal class AttributeTypeRequiredException : ArgumentException, IElikaException
{
    private static readonly string _msg = "Значение не является атрибутом";

    /// <summary>
    ///     Инициализурует исключение типа <see cref="AttributeTypeRequiredException" />
    /// </summary>
    public AttributeTypeRequiredException() : base(_msg)
    {
    }

    /// <summary>
    ///     Инициализурует исключение типа <see cref="AttributeTypeRequiredException" /> c указанием наименования аргумента
    /// </summary>
    /// <param name="paramName">Наименование аргумента</param>
    public AttributeTypeRequiredException(string paramName) : base(_msg, paramName)
    {
    }
}