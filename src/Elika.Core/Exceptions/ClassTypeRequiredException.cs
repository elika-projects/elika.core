﻿using System;

namespace Elika.Exceptions;

/// <summary>
///     Исключение, которое создаётся если тип не является классом
/// </summary>
public class ClassTypeRequiredException : ArgumentException, IElikaException
{
    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="ClassTypeRequiredException" /> с указанием параметра
    /// </summary>
    /// <param name="paramName">Наименование параметра</param>
    public ClassTypeRequiredException(string paramName) : base("Тип должен быть классом", paramName)
    {
    }
}