﻿using System;

namespace Elika.Exceptions;

/// <summary>
///     Исключение, которое создаётся если тип не является интерфейсом
/// </summary>
public class InterfaceTypeRequiredException : ArgumentException, IElikaException
{
    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="InterfaceTypeRequiredException" /> с указанием параметра
    /// </summary>
    /// <param name="paramName">Наименование параметра</param>
    public InterfaceTypeRequiredException(string paramName) : base("Тип должен быть интерфейсом", paramName)
    {
    }
}