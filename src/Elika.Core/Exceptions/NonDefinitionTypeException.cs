﻿using System;

namespace Elika.Exceptions;

/// <summary>
///     Исключение, которое создаётся если тип не является определяющим
/// </summary>
public class NonDefinitionTypeException : ArgumentException, IElikaException
{
    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="NonDefinitionTypeException" /> с указанием параметра
    /// </summary>
    /// <param name="paramName">Наименование параметра</param>
    public NonDefinitionTypeException(string paramName) : base("Не является определяющим типом", paramName)
    {
    }
}