﻿using System;

namespace Elika.Exceptions
{
    /// <summary>
    ///     Исключение, которое создаётся когда не найдено свободных портов
    /// </summary>
    public class NotFreePortException : Exception, IElikaException
    {
        private static readonly string _msg = "Нет доступных портов";

        /// <summary>
        ///     Инициализирует исключение типа <see cref="NotFreePortException"/>
        /// </summary>
        public NotFreePortException() : base(_msg)
        { }
    }
}
