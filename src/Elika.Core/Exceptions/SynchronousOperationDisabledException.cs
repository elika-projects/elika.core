﻿using System;

namespace Elika.Exceptions;

/// <summary>
///     Исключениеб которое создаётся когда используется отключённая синхронная операция
/// </summary>
public class SynchronousOperationDisabledException : InvalidOperationException
{
    /// <summary>
    ///     Инициализурует исключение типа <see cref="SynchronousOperationDisabledException" />
    /// </summary>
    public SynchronousOperationDisabledException() : base("Синхронная операция отключена.")
    {
    }
}