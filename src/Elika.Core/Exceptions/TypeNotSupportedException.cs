﻿using System;

namespace Elika.Exceptions;

/// <summary>
///     Исключение, которое создаётся если передан неверный тип
/// </summary>
public class TypeNotSupportedException : ArgumentException, IElikaException
{
    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="TypeNotSupportedException" /> с указанием параметра
    /// </summary>
    /// <param name="paramName">Наименование параметра</param>
    public TypeNotSupportedException(string paramName) : base("Поддерживаются только классы и интерфейсы",
        paramName)
    {
    }
}