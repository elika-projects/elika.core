﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO
{
    /// <summary>
    ///     Бинарный буфер
    /// </summary>
    public abstract class ElikaBinaryBuffer
    {

        private readonly Stream _stream;

        private byte[] _buffer;

        /// <summary>
        ///     Инициализация бинарного буфера
        /// </summary>
        /// <param name="stream">Поток данных</param>
        /// <param name="bufferSize">Размер буфера</param>
        /// <param name="isFixedSize">Флаг фиксированного размера буфера</param>
        protected ElikaBinaryBuffer(Stream stream, int bufferSize, bool isFixedSize = false) : this(stream, bufferSize, int.MaxValue, true)
        {
        }

        /// <summary>
        ///     Инициализация бинарного буфера
        /// </summary>
        /// <param name="stream">Поток данных</param>
        /// <param name="bufferSize">Размер буфера</param>
        /// <param name="maxBufferSize">Максимальный размер буфера</param>
        protected ElikaBinaryBuffer(Stream stream, int bufferSize, int maxBufferSize = int.MaxValue) : this(stream, bufferSize, maxBufferSize, false)
        {
        }

        private ElikaBinaryBuffer(Stream stream, int bufferSize, int maxBufferSize, bool isFixedSize)
        {
            MaxBufferSize = maxBufferSize;
            IsFixedSize = isFixedSize;
            _stream = stream;
            _buffer = new byte[bufferSize];
        }

        /// <summary>
        ///     Текущий размер буфера
        /// </summary>
        public int BufferSize => _buffer.Length;

        /// <summary>
        ///     Максимальный размер буфера
        /// </summary>
        public int MaxBufferSize { get; }

        /// <summary>
        ///     Флаг фиксированного размера буфера
        /// </summary>
        public bool IsFixedSize { get; }

        /// <summary>
        ///     Последний сегмент буфера
        /// </summary>
        protected ArraySegment<byte> LastBufferSegment { get; private set; }

        /// <summary>
        ///     Заполнить буфер данными
        /// </summary>
        /// <param name="count">Кол-во байт</param>
        /// <param name="cancellationToken">Токен отмены задачи</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void FillBuffer(int count, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();   
            ResizeBufferIfShort(count);

            LastBufferSegment = _stream.ReadBytesExactly(_buffer, 0, count, cancellationToken);
            OnBufferFilled(LastBufferSegment);
        }

        /// <summary>
        ///     Асинхронно заполнить буфер данными
        /// </summary>
        /// <param name="count">Кол-во байт</param>
        /// <param name="cancellationToken">Токен отмены задачи</param>
        /// <returns>Задача</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected async Task FillBufferAsync(int count, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ResizeBufferIfShort(count);

            LastBufferSegment = await _stream.ReadBytesExactlyAsync(_buffer, 0, count, cancellationToken);
            OnBufferFilled(LastBufferSegment);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ResizeBufferIfShort(int count)
        {
            if (_buffer.Length > count)
                return;

            if (IsFixedSize)
                throw new InvalidOperationException("Невозможно изменить размер буфера, указан фиксированный размер");

            if (count > MaxBufferSize)
                throw new InvalidOperationException("Превышен максимальный размер буфера");

            if (_buffer.Length < count)
                Array.Resize(ref _buffer, count);
        }

        /// <summary>
        ///     Вызывается когда буфер данных заполнен
        /// </summary>
        protected virtual void OnBufferFilled(ArraySegment<byte> segment)
        {}
    }
}
