﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Бинарный считыватель
/// </summary>
public class ElikaBinaryReader : ElikaBinaryBuffer
{
    /// <summary>
    ///     Инициализация считывателя
    /// </summary>
    /// <param name="stream">Поток данных</param>
    /// <param name="bufferSize"></param>
    /// <param name="maxBufferSize">Максимальный размер буфера</param>
    public ElikaBinaryReader(Stream stream, int bufferSize = 16, int maxBufferSize = int.MaxValue) : base(stream, bufferSize, maxBufferSize)
    {
        ArgumentNullException.ThrowIfNull(stream);
        Stream = stream;
    }

    /// <summary>
    ///     Поток данных
    /// </summary>
    public Stream Stream { get; }

    /// <summary>
    ///     Прочитать байт
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Байт</returns>
    public byte ReadByte(CancellationToken cancellationToken = default)
    {
        FillBuffer(1, cancellationToken);
        return LastBufferSegment[0];
    }

    /// <summary>
    ///     Асинхронно прочитать байт
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения байта</returns>
    public async Task<byte> ReadByteAsync(CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(1, cancellationToken);
        return LastBufferSegment[0];
    }

    /// <summary>
    ///     Гарантированно прочитать данные
    /// </summary>
    /// <param name="buffer">Буффер данных</param>
    /// <param name="startPos">Начальная позиция записи в буффер</param>
    /// <param name="count">Кол-во байт для чтения</param>
    /// <param name="cancellationToken">Токен отмены</param>
    public void ReadExactly(byte[] buffer, int startPos, int count, CancellationToken cancellationToken = default)
    {
        FillBuffer(count, cancellationToken);
        Buffer.BlockCopy(LastBufferSegment.Array!, LastBufferSegment.Offset, buffer, startPos, count);
    }

    /// <summary>
    ///     Асинхронно гарантированно прочитать данные
    /// </summary>
    /// <param name="buffer">Буффер данных</param>
    /// <param name="startPos">Начальная позиция записи в буффер</param>
    /// <param name="count">Кол-во байт для чтения</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения</returns>
    public async Task ReadExactlyAsync(byte[] buffer, int startPos, int count,
        CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(count, cancellationToken);
        Buffer.BlockCopy(LastBufferSegment.Array!, LastBufferSegment.Offset, buffer, startPos, count);
    }

    /// <summary>
    ///     Прочитать <see cref="ushort" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    public ushort ReadUInt16(CancellationToken cancellationToken = default)
    {
        FillBuffer(2, cancellationToken);
        return BitConverter.ToUInt16(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="ushort" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public async Task<ushort> ReadUInt16Async(CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(2, cancellationToken);
        return BitConverter.ToUInt16(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Прочитать <see cref="short" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    public short ReadInt16(CancellationToken cancellationToken = default)
    {
        FillBuffer(2, cancellationToken);
        return BitConverter.ToInt16(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="ushort" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public async Task<short> ReadInt16Async(CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(2, cancellationToken);
        return BitConverter.ToInt16(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Прочитать <see cref="uint" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    public uint ReadUInt32(CancellationToken cancellationToken = default)
    {
        FillBuffer(4, cancellationToken);
        return BitConverter.ToUInt32(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="ushort" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public async Task<uint> ReadUInt32Async(CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(4, cancellationToken);
        return BitConverter.ToUInt32(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Прочитать <see cref="int" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    public int ReadInt32(CancellationToken cancellationToken = default)
    {
        FillBuffer(4, cancellationToken);
        return BitConverter.ToInt32(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="ushort" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public async Task<int> ReadInt32Async(CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(4, cancellationToken);
        return BitConverter.ToInt32(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Прочитать <see cref="ulong" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    public ulong ReadUInt64(CancellationToken cancellationToken = default)
    {
        FillBuffer(8, cancellationToken);
        return BitConverter.ToUInt64(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="ushort" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public async Task<ulong> ReadUInt64Async(CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(8, cancellationToken);
        return BitConverter.ToUInt64(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Прочитать <see cref="long" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    public long ReadInt64(CancellationToken cancellationToken = default)
    {
        FillBuffer(8, cancellationToken);
        return BitConverter.ToInt64(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="ushort" />
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public async Task<long> ReadInt64Async(CancellationToken cancellationToken = default)
    {
        await FillBufferAsync(4, cancellationToken);
        return BitConverter.ToInt64(LastBufferSegment.Array!, LastBufferSegment.Offset);
    }

    /// <summary>
    ///     Прочитать <see cref="string" /> в кодировке ASCII
    /// </summary>
    /// <param name="count">Кол-во байт</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    public string ReadString(int count, CancellationToken cancellationToken = default)
    {
        return ReadString(count, Encoding.ASCII, cancellationToken);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="string" /> в кодировке ASCII
    /// </summary>
    /// <param name="count">Кол-во байт</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public Task<string> ReadStringAsync(int count, CancellationToken cancellationToken = default)
    {
        return ReadStringAsync(count, Encoding.ASCII, cancellationToken);
    }

    /// <summary>
    ///     Прочитать <see cref="string" /> в указанной кодировке
    /// </summary>
    /// <param name="count">Кол-во байт</param>
    /// <param name="encoding">Кодировка</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Значение</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public string ReadString(int count, Encoding encoding, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(encoding);

        FillBuffer(count, cancellationToken);
        return encoding.GetString(LastBufferSegment.Array!, LastBufferSegment.Offset, count);
    }

    /// <summary>
    ///     Асинхронно прочитать <see cref="string" /> в указанной кодировке
    /// </summary>
    /// <param name="count">Кол-во байт</param>
    /// <param name="encoding">Кодировка</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public async Task<string> ReadStringAsync(int count, Encoding encoding = null,
        CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(encoding);

        await FillBufferAsync(count, cancellationToken);
        return encoding.GetString(LastBufferSegment.Array!, LastBufferSegment.Offset, count);
    }
}