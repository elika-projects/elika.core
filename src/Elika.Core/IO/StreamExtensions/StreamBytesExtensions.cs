﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи массива байт для <see cref="Stream" />
/// </summary>
public static class StreamBytesExtensions
{
    /// <summary>
    ///     Гарантированно прочитать кол-во байт из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="buffer">Целевой буфер</param>
    /// <param name="offset">Смещения с которого начать запись в буфер</param>
    /// <param name="count">Кол-во байтов для чтения</param>
    /// <param name="cancellationToken">Токен отмены (работает только если поток поддерживает ожидание)</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ArraySegment<byte> ReadBytesExactly(this Stream stream, byte[] buffer, int offset, int count,
        CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(stream);

        var currentOffset = offset;
        while (currentOffset < count)
        {
            var canTimeout = stream.CanTimeout;
            var read = stream.Read(buffer, currentOffset, count - currentOffset);

            if (!canTimeout && read == 0)
                throw new EndOfStreamException();

            if (canTimeout)
                cancellationToken.ThrowIfCancellationRequested();

            currentOffset += read;
        }

        return new ArraySegment<byte>(buffer, offset, count);
    }

    /// <summary>
    ///     Гарантированно прочитать кол-во байт из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="buffer">Целевой буфер</param>
    /// <param name="cancellationToken">Токен отмены (работает только если поток поддерживает ожидание)</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ReadBytesExactly(this Stream stream, ArraySegment<byte> buffer,
        CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(buffer.Array);
        stream.ReadBytesExactly(buffer.Array, buffer.Offset, buffer.Count, cancellationToken);
    }

    /// <summary>
    ///     Гарантированно прочитать кол-во байт из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="count">Кол-во байтов для чтения</param>
    /// <param name="cancellationToken">Токен отмены (работает только если поток поддерживает ожидание)</param>
    /// <returns>Массив байт</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static byte[] ReadBytesExactly(this Stream stream, int count, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(stream);
        var buffer = new byte[count];
        stream.ReadBytesExactly(buffer, 0, count, cancellationToken);
        return buffer;
    }

    /// <summary>
    ///     Гарантированно прочитать байт из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены (работает только если поток поддерживает ожидание)</param>
    /// <returns>Байт</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static byte ReadByteExactly(this Stream stream, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(stream);
        return stream.ReadBytesExactly(1, cancellationToken)[0];
    }

    /// <summary>
    ///     Асинхронно гарантированно прочитать кол-во байт из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="buffer">Целевой буфер</param>
    /// <param name="offset">Смещения с которого начать запись в буфер</param>
    /// <param name="count">Кол-во байтов для чтения</param>
    /// <param name="cancellationToken">Токен отмены (работает только если поток поддерживает ожидание)</param>
    /// <returns>Задача чтения</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static async Task<ArraySegment<byte>> ReadBytesExactlyAsync(this Stream stream, byte[] buffer, int offset, int count,
        CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(stream);

        var currentOffset = offset;
        while (currentOffset < count)
        {
            var canTimeout = stream.CanTimeout;
            var read = await stream.ReadAsync(buffer, currentOffset, count - currentOffset, cancellationToken);

            if (!canTimeout && read == 0)
                throw new EndOfStreamException();

            if (canTimeout)
                cancellationToken.ThrowIfCancellationRequested();

            currentOffset += read;
        }

        return new ArraySegment<byte>(buffer, offset, count);
    }

    /// <summary>
    ///     Асинхронно гарантированно прочитать кол-во байт из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="buffer">Целевой буфер</param>
    /// <param name="cancellationToken">Токен отмены (работает только если поток поддерживает ожидание)</param>
    /// <returns>Задача чтения</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Task ReadBytesExactlyAsync(this Stream stream, ArraySegment<byte> buffer,
        CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(buffer.Array);

        return stream.ReadBytesExactlyAsync(buffer.Array, buffer.Offset, buffer.Count, cancellationToken);
    }

    /// <summary>
    ///     Асинхронно гарантированно прочитать кол-во байт из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="count">Кол-во байтов</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения массива байт</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static async Task<byte[]> ReadBytesExactlyAsync(this Stream stream, int count,
        CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(stream);

        var buffer = new byte[count];
        await stream.ReadBytesExactlyAsync(buffer, 0, count, cancellationToken);
        return buffer;
    }

    /// <summary>
    ///     Прочитать байт асинхронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <returns>Задача чтения байта</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static async Task<byte> ReadByteExactlyAsync(this Stream stream)
    {
        return (await stream.ReadBytesExactlyAsync(1))[0];
    }
}