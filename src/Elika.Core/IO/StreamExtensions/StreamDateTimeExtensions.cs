﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи <see cref="DateTime" /> для <see cref="Stream" />
/// </summary>
public static class StreamDateTimeExtensions
{
    /// <summary>
    ///     Прочитать из потока значение типа <see cref="DateTime" /> в Unix формате
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <returns>Значение</returns>
    public static DateTime ReadUnixTimeStamp(this Stream stream)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = stream.ReadInt32();
        return UnixTime.ToDateTime(buff);
    }

    /// <summary>
    ///     Прочитать из потока значение типа <see cref="DateTime" /> в Unix формате асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public static async Task<DateTime> ReadUnixTimeStampAsync(this Stream stream,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = await stream.ReadInt32Async(cancellationToken);
        return UnixTime.ToDateTime(buff);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="DateTime" /> в Unix формате
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    public static void WriteAsUnixTime(this Stream stream, DateTime value)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = UnixTime.ToUnixTimeSeconds(value);
        stream.Write(buff);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="DateTime" /> в Unix формате асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача записи значения</returns>
    public static ValueTask WriteAsUnixTimeAsync(this Stream stream, DateTime value,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = UnixTime.ToUnixTimeSeconds(value);
        return stream.WriteAsync(buff, cancellationToken);
    }
}