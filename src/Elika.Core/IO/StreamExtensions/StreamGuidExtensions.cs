﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи <see cref="Guid" /> для <see cref="Stream" />
/// </summary>
public static class StreamGuidExtensions
{
    /// <summary>
    ///     Прочитать из потока значение типа <see cref="Guid" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <returns>Значение</returns>
    public static Guid ReadGuid(this Stream stream)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var bytes = stream.ReadBytesExactly(16);
        var guid = new Guid(bytes);
        return guid;
    }

    /// <summary>
    ///     Прочитать из потока значение типа <see cref="Guid" /> асинхронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public static async Task<Guid> ReadGuidAsync(this Stream stream,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var bytes = await stream.ReadBytesExactlyAsync(16, cancellationToken);
        var guid = new Guid(bytes);
        return guid;
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="Guid" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    public static void Write(this Stream stream, Guid value)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = value.ToByteArray();
        stream.Write(buff);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="Guid" /> асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача записи значения</returns>
    public static ValueTask WriteAsync(this Stream stream, Guid value,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = value.ToByteArray();
        return stream.WriteAsync(buff, cancellationToken);
    }
}