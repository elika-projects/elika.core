﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи <see cref="short" /> для <see cref="Stream" />
/// </summary>
public static class StreamInt16Extensions
{
    /// <summary>
    ///     Прочитать из потока значение типа <see cref="short" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <returns>Значение</returns>
    public static short ReadInt16(this Stream stream)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = stream.ReadBytesExactly(2);
        return BitConverter.ToInt16(buff, 0);
    }

    /// <summary>
    ///     Прочитать из потока значение типа <see cref="short" /> асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public static async Task<short> ReadInt16Async(this Stream stream,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = await stream.ReadBytesExactlyAsync(2, cancellationToken);
        return BitConverter.ToInt16(buff, 0);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="short" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    public static void Write(this Stream stream, short value)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        stream.Write(buff);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="short" /> асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача записи значения</returns>
    public static ValueTask WriteAsync(this Stream stream, short value,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        return stream.WriteAsync(buff, cancellationToken);
    }
}