﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи <see cref="int" /> для <see cref="Stream" />
/// </summary>
public static class StreamInt32Extensions
{
    /// <summary>
    ///     Прочитать из потока значение типа <see cref="int" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <returns>Значение</returns>
    public static int ReadInt32(this Stream stream)
    {
        var buff = stream.ReadBytesExactly(4);
        return BitConverter.ToInt32(buff, 0);
    }

    /// <summary>
    ///     Асинхронно прочитать из потока значение типа <see cref="int" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public static async Task<int> ReadInt32Async(this Stream stream,
        CancellationToken cancellationToken = default)
    {
        var buff = await stream.ReadBytesExactlyAsync(4, cancellationToken);
        return BitConverter.ToInt32(buff, 0);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="int" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    public static void Write(this Stream stream, int value)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        stream.Write(buff);
    }

    /// <summary>
    ///     Асинхронно записать в поток значение типа <see cref="int" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача записи значения</returns>
    public static ValueTask WriteAsync(this Stream stream, int value,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        return stream.WriteAsync(buff, cancellationToken);
    }
}