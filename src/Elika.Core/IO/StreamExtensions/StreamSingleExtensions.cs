﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи <see cref="float" /> для <see cref="Stream" />
/// </summary>
public static class StreamSingleExtensions
{
    /// <summary>
    ///     Прочитать из потока значение типа <see cref="float" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <returns>Значение</returns>
    public static float ReadSingle(this Stream stream)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = stream.ReadBytesExactly(4);
        return BitConverter.ToSingle(buff, 0);
    }

    /// <summary>
    ///     Прочитать из потока значение типа <see cref="uint" /> асинхронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public static async Task<float> ReadSingleAsync(this Stream stream,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = await stream.ReadBytesExactlyAsync(4, cancellationToken);
        return BitConverter.ToSingle(buff, 0);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="float" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    public static void Write(this Stream stream, float value)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        stream.Write(buff);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="float" /> асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача записи значения</returns>
    public static ValueTask WriteAsync(this Stream stream, float value,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        return stream.WriteAsync(buff, cancellationToken);
    }
}