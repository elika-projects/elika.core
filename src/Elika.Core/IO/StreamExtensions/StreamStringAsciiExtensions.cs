﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи <see cref="string" /> в кодировке ASCII для <see cref="Stream" />
/// </summary>
public static class StreamStringAsciiExtensions
{
    /// <summary>
    ///     Прочитать из потока значение типа <see cref="string" /> в кодировке ASCII
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="count">Кол-во символов</param>
    /// <returns>Значение</returns>
    public static string ReadAsciiString(this Stream stream, int count)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = stream.ReadBytesExactly(count);
        var str = Encoding.ASCII.GetString(buff);
        return str;
    }

    /// <summary>
    ///     Прочитать из потока значение типа <see cref="string" /> в кодировке ASCII асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="count">Кол-во символов</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public static async Task<string> ReadAsciiStringAsync(this Stream stream, int count,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = await stream.ReadBytesExactlyAsync(count, cancellationToken);
        var str = Encoding.ASCII.GetString(buff);
        return str;
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="string" /> в кодировке ASCII асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача записи значения</returns>
    public static ValueTask WriteAsAsciiAsync(this Stream stream, string value,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = Encoding.ASCII.GetBytes(value);
        return stream.WriteAsync(buff, cancellationToken);
    }
}