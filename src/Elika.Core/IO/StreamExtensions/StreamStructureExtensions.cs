﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Elika.Runtime.InteropServices;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи структур для <see cref="Stream" />
/// </summary>
public static class StreamStructureExtensions
{
    /// <summary>
    ///     Чтение структуры из потока
    /// </summary>
    /// <typeparam name="T">Тип структуры</typeparam>
    /// <param name="stream">Поток</param>
    /// <returns>Структура</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    /// <exception cref="IOException"></exception>
    /// <exception cref="NotSupportedException"></exception>
    /// <exception cref="ObjectDisposedException"></exception>
    public static T ReadStructure<T>(this Stream stream) where T : struct
    {
        return (T) stream.ReadStructure(typeof(T));
    }

    /// <summary>
    ///     Чтение структуры из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="dataType">Тип структуры</param>
    /// <returns>Структура</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    /// <exception cref="IOException"></exception>
    /// <exception cref="NotSupportedException"></exception>
    /// <exception cref="ObjectDisposedException"></exception>
    public static object ReadStructure(this Stream stream, Type dataType)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        if (dataType == null)
            throw new ArgumentNullException(nameof(dataType));

        var size = Marshal.SizeOf(dataType);
        var buffer = stream.ReadBytesExactly(size);
        return MarshalTools.BytesToStructure(buffer, dataType);
    }

    /// <summary>
    ///     Асихнронное чтение структуры из потока
    /// </summary>
    /// <typeparam name="T">Тип структуры</typeparam>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Асихронная задача структуры</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    /// <exception cref="IOException"></exception>
    public static async Task<T> ReadStructureAsync<T>(this Stream stream,
        CancellationToken cancellationToken = default) where T : struct
    {
        return (T) await stream.ReadStructureAsync(typeof(T), cancellationToken);
    }

    /// <summary>
    ///     Асихнронное чтение структуры из потока
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="dataType">Тип структуры</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Асихронная задача структуры</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    /// <exception cref="IOException"></exception>
    public static async Task<object> ReadStructureAsync(this Stream stream, Type dataType,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        if (dataType == null)
            throw new ArgumentNullException(nameof(dataType));

        var size = Marshal.SizeOf(dataType);
        var buffer = await stream.ReadBytesExactlyAsync(size, cancellationToken);
        return MarshalTools.BytesToStructure(buffer, dataType);
    }

    /// <summary>
    ///     Запись структуры в поток
    /// </summary>
    /// <typeparam name="T">Тип структуры</typeparam>
    /// <param name="stream">Поток</param>
    /// <param name="obj">Структура</param>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    public static void WriteStructure<T>(this Stream stream, T obj) where T : struct
    {
        stream.WriteStructure((object) obj);
    }

    /// <summary>
    ///     Запись структуры в поток
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="obj">Структура</param>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    public static void WriteStructure(this Stream stream, object obj)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        if (obj == null)
            throw new ArgumentNullException(nameof(obj));

        var buffer = MarshalTools.StructureToBytes(obj);
        stream.Write(buffer);
    }

    /// <summary>
    ///     Асинхронная запись структуры в поток
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="obj">Структура</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Асихронная задача</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    /// <exception cref="NotSupportedException"></exception>
    /// <exception cref="ObjectDisposedException"></exception>
    public static ValueTask WriteStructureAsync(this Stream stream, object obj,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        if (obj == null)
            throw new ArgumentNullException(nameof(obj));

        var buffer = MarshalTools.StructureToBytes(obj);
        return stream.WriteAsync(buffer, cancellationToken);
    }
}