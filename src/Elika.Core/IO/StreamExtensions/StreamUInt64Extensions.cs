﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.IO;

/// <summary>
///     Расширение для чтения и записи <see cref="ulong" /> для <see cref="Stream" />
/// </summary>
public static class StreamUInt64Extensions
{
    /// <summary>
    ///     Прочитать из потока значение типа <see cref="ulong" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <returns>Значение</returns>
    public static ulong ReadUInt64(this Stream stream)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = stream.ReadBytesExactly(8);
        return BitConverter.ToUInt64(buff, 0);
    }

    /// <summary>
    ///     Прочитать из потока значение типа <see cref="ulong" />  асинхронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача чтения значения</returns>
    public static async Task<ulong> ReadUInt64Async(this Stream stream,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = await stream.ReadBytesExactlyAsync(8, cancellationToken);
        return BitConverter.ToUInt64(buff, 0);
    }

    /// <summary>
    ///     Записать в поток значение типа <see cref="ushort" />
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    public static void Write(this Stream stream, ulong value)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        stream.Write(buff);
    }


    /// <summary>
    ///     Записать в поток значение типа <see cref="ulong" /> асихнронно
    /// </summary>
    /// <param name="stream">Поток</param>
    /// <param name="value">Значение</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача записи значения</returns>
    public static ValueTask WriteAsync(this Stream stream, ulong value,
        CancellationToken cancellationToken = default)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        var buff = BitConverter.GetBytes(value);
        return stream.WriteAsync(buff, cancellationToken);
    }
}