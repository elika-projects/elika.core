﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elika.Collection.Generic;

namespace Elika.Linq;

/// <summary>
///     Расширения для <see cref="IEnumerable{T}" />
/// </summary>
public static class EnumerableExtensions
{
    /// <summary>
    ///     Выполнить foreach по коллекции
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="action">Действие над элементом</param>
    public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
    {
        source.ForEach((source1, i) => action(source1));
    }

    /// <summary>
    ///     Выполнить foreach по коллекции
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="action">Действие над элементом c указанием индекса</param>
    public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource, int> action)
    {
        source.ForEach((current, i, prev) => action(current, i));
    }

    /// <summary>
    ///     Выполнить foreach по коллекции
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="action">Действие над элементом c указанием индекса и предыдущего элемента</param>
    public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource, int, TSource> action)
    {
        ArgumentNullException.ThrowIfNull(source);
        ArgumentNullException.ThrowIfNull(action);

        TSource prev = default;
        var index = 0;
        foreach (var current in source)
        {
            action(current, index, prev);
            prev = current;
            index++;
        }
    }

    /// <summary>
    ///     Выполнить сортировку в алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="keySelector">Функция получения ключа</param>
    /// <returns>Сортированная коллекция</returns>
    public static IOrderedEnumerable<TSource> NaturalOrderBy<TSource>(this IEnumerable<TSource> source,
        Func<TSource, string> keySelector)
    {
        return source.OrderBy(keySelector, new NaturalComparer());
    }

    /// <summary>
    ///     Сортировка в обратном алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="keySelector">Функция получения ключа</param>
    /// <returns>Сортированная коллекция</returns>
    public static IOrderedEnumerable<TSource> NaturalOrderByDescending<TSource>(this IEnumerable<TSource> source,
        Func<TSource, string> keySelector)
    {
        return source.OrderByDescending(keySelector, new NaturalComparer());
    }

    #region MaxOrDefault

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal MaxOrDefault(this IEnumerable<decimal> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double MaxOrDefault(this IEnumerable<double> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int MaxOrDefault(this IEnumerable<int> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long MaxOrDefault(this IEnumerable<long> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal? MaxOrDefault(this IEnumerable<decimal?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double? MaxOrDefault(this IEnumerable<double?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int? MaxOrDefault(this IEnumerable<int?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long? MaxOrDefault(this IEnumerable<long?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float? MaxOrDefault(this IEnumerable<float?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float MaxOrDefault(this IEnumerable<float> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static TSource MaxOrDefault<TSource>(this IEnumerable<TSource> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <typeparam name="TResult">Тип результата</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static TResult MaxOrDefault<TSource, TResult>(this IEnumerable<TSource> source,
        Func<TSource, TResult> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    #endregion

    #region MinOrDefault

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal MinOrDefault(this IEnumerable<decimal> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double MinOrDefault(this IEnumerable<double> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int MinOrDefault(this IEnumerable<int> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long MinOrDefault(this IEnumerable<long> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal? MinOrDefault(this IEnumerable<decimal?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double? MinOrDefault(this IEnumerable<double?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int? MinOrDefault(this IEnumerable<int?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long? MinOrDefault(this IEnumerable<long?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float? MinOrDefault(this IEnumerable<float?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float MinOrDefault(this IEnumerable<float> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static TSource MinOrDefault<TSource>(this IEnumerable<TSource> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <typeparam name="TResult">Тип результата</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static TResult MinOrDefault<TSource, TResult>(this IEnumerable<TSource> source,
        Func<TSource, TResult> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    #endregion
}