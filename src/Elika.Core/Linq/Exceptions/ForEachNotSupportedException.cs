﻿using System;
using System.Linq;

namespace Elika.Linq.Exceptions;

/// <summary>
///     Ислючение использования метода ForEach в <seealso cref="IQueryable" />
/// </summary>
internal class ForEachNotSupportedException : NotSupportedException, IElikaException
{
    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="ForEachNotSupportedException" />
    /// </summary>
    public ForEachNotSupportedException() : base(
        "Метод ForEach не поддерживаются в 'IQueryable'")
    {
    }
}