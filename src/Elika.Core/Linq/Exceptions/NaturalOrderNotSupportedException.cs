﻿using System;
using System.Linq;

namespace Elika.Linq.Exceptions;

/// <summary>
///     Ислючение использования метода натуральной сортировки в <seealso cref="IQueryable" />
/// </summary>
internal class NaturalOrderNotSupportedException : NotSupportedException, IElikaException
{
    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="NaturalOrderNotSupportedException" />
    /// </summary>
    public NaturalOrderNotSupportedException() : base(
        "Методы натуральной сортировки не поддерживаются в 'IQueryable'")
    {
    }
}