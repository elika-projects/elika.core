﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Elika.Linq.Expressions;

/// <summary>
///     Расширения для <see cref="Expression" />
/// </summary>
public static class ExpressionExtensions
{
    /// <summary>
    ///     Получить наименования свойства/поля из выражения
    /// </summary>
    /// <param name="expr">Выражение</param>
    /// <returns>Наименования</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string[] GetPropertyNames(this Expression expr)
    {
        if (expr == null)
            throw new ArgumentNullException(nameof(expr));

        if (expr is NewExpression newExpr)
            return newExpr.GetPropertyNames();

        if (expr is MemberExpression memberExpr)
            return memberExpr.GetPropertyNames();

        if (expr is LambdaExpression lambdaExpr)
            return lambdaExpr.GetPropertyNames();

        if (expr is UnaryExpression unaryExpr)
            return unaryExpr.GetPropertyNames();

        return null;
    }

    /// <summary>
    ///     Получить наименования свойства/поля из выражения типа <see cref="NewExpression" />
    /// </summary>
    /// <param name="expr">Выражение</param>
    /// <returns>Наименования</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string[] GetPropertyNames(this NewExpression expr)
    {
        if (expr == null)
            throw new ArgumentNullException(nameof(expr));

        var names = expr.Arguments
            .SelectMany(expression => expression.GetPropertyNames()).ToArray();
        return names;
    }

    /// <summary>
    ///     Получить наименования свойства/поля из выражения типа <see cref="MemberExpression" />
    /// </summary>
    /// <param name="expr">Выражение</param>
    /// <returns>Наименования</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string[] GetPropertyNames(this MemberExpression expr)
    {
        if (expr == null)
            throw new ArgumentNullException(nameof(expr));

        var name = expr.Member.Name;
        return new[] {name};
    }

    /// <summary>
    ///     Получить наименования свойства/поля из выражения типа <see cref="LambdaExpression" />
    /// </summary>
    /// <param name="expr">Выражение</param>
    /// <returns>Наименования</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string[] GetPropertyNames(this LambdaExpression expr)
    {
        if (expr == null)
            throw new ArgumentNullException(nameof(expr));

        return expr.Body.GetPropertyNames();
    }

    /// <summary>
    ///     Получить наименования свойства/поля из выражения типа <see cref="UnaryExpression" />
    /// </summary>
    /// <param name="expr">Выражение</param>
    /// <returns>Наименования</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string[] GetPropertyNames(this UnaryExpression expr)
    {
        if (expr == null)
            throw new ArgumentNullException(nameof(expr));

        return expr.Operand.GetPropertyNames();
    }
}