﻿using System;
using System.Linq;
using Elika.Collection.Generic;

namespace Elika.Linq;

/// <summary>
///     Расширение для <see cref="IOrderedEnumerable{TElement}" />
/// </summary>
public static class OrderedEnumerableExtensions
{
    /// <summary>
    ///     Сортировка в алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="keySelector">Функция получения ключа</param>
    /// <returns>Сортированная коллекция</returns>
    public static IOrderedEnumerable<TSource> NaturalThenBy<TSource>(this IOrderedEnumerable<TSource> source,
        Func<TSource, string> keySelector)
    {
        return source.ThenBy(keySelector, new NaturalComparer());
    }

    /// <summary>
    ///     Сортировка в обратном алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="keySelector">Функция получения ключа</param>
    /// <returns>Сортированная коллекция</returns>
    public static IOrderedEnumerable<TSource> NaturalThenByDescending<TSource>(
        this IOrderedEnumerable<TSource> source, Func<TSource, string> keySelector)
    {
        return source.ThenByDescending(keySelector, new NaturalComparer());
    }
}