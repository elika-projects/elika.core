﻿using System;
using System.Linq;
using Elika.Linq.Exceptions;

namespace Elika.Linq;

/// <summary>
///     Расширения для <see cref="IOrderedQueryable{T}" />
/// </summary>
public static class OrderedQueryableExtensions
{
    /// <summary>
    ///     Сортировка в алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <see cref="NaturalOrderNotSupportedException" />
    public static IOrderedQueryable<TSource> NaturalThenBy<TSource>(this IOrderedQueryable<TSource> source,
        Func<TSource, string> keySelector)
    {
        throw new NaturalOrderNotSupportedException();
    }

    /// <summary>
    ///     Сортировка в обратном алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <see cref="NaturalOrderNotSupportedException" />
    public static IOrderedQueryable<TSource> NaturalThenByDescending<TSource>(
        this IOrderedQueryable<TSource> source, Func<TSource, string> keySelector)
    {
        throw new NaturalOrderNotSupportedException();
    }
}