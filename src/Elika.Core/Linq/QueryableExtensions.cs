﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Elika.Linq.Exceptions;

namespace Elika.Linq;

/// <summary>
///     Расширения для <see cref="IQueryable{T}" />
/// </summary>
public static class QueryableExtensions
{
    /// <summary>
    ///     Только уникальные значения по полю
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static IQueryable<TSource> DistinctBy<TSource, TKey>(this IQueryable<TSource> source,
        Expression<Func<TSource, TKey>> keySelector)
    {
        return source.GroupBy(keySelector).Select(sources => sources.First());
    }

    /// <summary>
    ///     Выполнить foreach по коллекции
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="action">Действие над элементом</param>
    public static void ForEach<TSource>(this IQueryable<TSource> source, Action<TSource> action)
    {
        throw new ForEachNotSupportedException();
    }

    /// <summary>
    ///     Выполнить foreach по коллекции
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="action">Действие над элементом c указанием индекса</param>
    public static void ForEach<TSource>(this IQueryable<TSource> source, Action<TSource, int> action)
    {
        throw new ForEachNotSupportedException();
    }

    /// <summary>
    ///     Выполнить foreach по коллекции
    /// </summary>
    /// <typeparam name="TSource">Тип элемента коллекции</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="action">Действие над элементом c указанием индекса и предыдущего элемента</param>
    public static void ForEach<TSource>(this IQueryable<TSource> source, Action<TSource, int, TSource> action)
    {
        throw new ForEachNotSupportedException();
    }

    /// <summary>
    ///     Сортировка в алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <see cref="NaturalOrderNotSupportedException" />
    public static IOrderedQueryable<TSource> NaturalOrderBy<TSource>(this IQueryable<TSource> source,
        Func<TSource, string> keySelector)
    {
        throw new NaturalOrderNotSupportedException();
    }

    /// <summary>
    ///     Сортировка в обратном алфавитном порядке, за исключением того, что числа упорядочены как одно целое
    /// </summary>
    /// <see cref="NaturalOrderNotSupportedException" />
    public static IOrderedQueryable<TSource> NaturalOrderByDescending<TSource>(this IQueryable<TSource> source,
        Func<TSource, string> keySelector)
    {
        throw new NaturalOrderNotSupportedException();
    }

    #region MaxOrDefault

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal MaxOrDefault(this IQueryable<decimal> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double MaxOrDefault(this IQueryable<double> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int MaxOrDefault(this IQueryable<int> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long MaxOrDefault(this IQueryable<long> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal? MaxOrDefault(this IQueryable<decimal?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double? MaxOrDefault(this IQueryable<double?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int? MaxOrDefault(this IQueryable<int?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long? MaxOrDefault(this IQueryable<long?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float? MaxOrDefault(this IQueryable<float?> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float MaxOrDefault(this IQueryable<float> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static TSource MaxOrDefault<TSource>(this IQueryable<TSource> source)
    {
        return source.DefaultIfEmpty().Max();
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, decimal> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, double> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, int> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, long> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static decimal? MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, decimal?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static double? MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, double?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static int? MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, int?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static long? MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, long?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float? MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, float?> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static float MaxOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, float> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    /// <summary>
    ///     Получить максимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <typeparam name="TResult">Тип результата</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Максимальное значение или по умолчанию</returns>
    public static TResult MaxOrDefault<TSource, TResult>(this IQueryable<TSource> source,
        Func<TSource, TResult> selector)
    {
        return source.DefaultIfEmpty().Max(selector);
    }

    #endregion

    #region MinOrDefault

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal MinOrDefault(this IQueryable<decimal> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double MinOrDefault(this IQueryable<double> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int MinOrDefault(this IQueryable<int> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long MinOrDefault(this IQueryable<long> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal? MinOrDefault(this IQueryable<decimal?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double? MinOrDefault(this IQueryable<double?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int? MinOrDefault(this IQueryable<int?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long? MinOrDefault(this IQueryable<long?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float? MinOrDefault(this IQueryable<float?> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float MinOrDefault(this IQueryable<float> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static TSource MinOrDefault<TSource>(this IQueryable<TSource> source)
    {
        return source.DefaultIfEmpty().Min();
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, decimal> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, double> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, int> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, long> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static decimal? MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, decimal?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static double? MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, double?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static int? MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, int?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static long? MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, long?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float? MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, float?> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static float MinOrDefault<TSource>(this IQueryable<TSource> source, Func<TSource, float> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    /// <summary>
    ///     Получить минимальное значение или по умолчанию
    /// </summary>
    /// <typeparam name="TSource">Тип значения</typeparam>
    /// <typeparam name="TResult">Тип результата</typeparam>
    /// <param name="source">Исходная коллекция</param>
    /// <param name="selector">Выбор поля для значения</param>
    /// <returns>Минимальное значение или по умолчанию</returns>
    public static TResult MinOrDefault<TSource, TResult>(this IQueryable<TSource> source,
        Func<TSource, TResult> selector)
    {
        return source.DefaultIfEmpty().Min(selector);
    }

    #endregion
}