﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Elika.Net.Sockets
{
    /// <summary>
    ///     Расширения для <see cref="Socket"/>
    /// </summary>
    public static class SocketExtensions
    {
        /// <summary>
        ///     Асинхронно прочитать гарантированное кол-во байт
        /// </summary>
        /// <param name="socket">Сокет</param>
        /// <param name="buffer">Буффер</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Задача чтения</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ValueTask ReadExactlyAsync(this Socket socket, ArraySegment<byte> buffer, CancellationToken cancellationToken = default)
        {
            return socket.ReadExactlyAsync(new Memory<byte>(buffer.Array, buffer.Offset, buffer.Count), buffer.Count, cancellationToken);
        }

        /// <summary>
        ///     Асинхронно прочитать гарантированное кол-во байт
        /// </summary>
        /// <param name="socket">Сокет</param>
        /// <param name="buffer">Буффер</param>
        /// <param name="count">Кол-во байт для чтения</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Задача чтения</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ValueTask ReadExactlyAsync(this Socket socket, ArraySegment<byte> buffer, int count, CancellationToken cancellationToken = default)
        {
            return socket.ReadExactlyAsync(new Memory<byte>(buffer.Array, buffer.Offset, buffer.Count), count, cancellationToken);
        }

        /// <summary>
        ///     Асинхронно прочитать гарантированное кол-во байт
        /// </summary>
        /// <param name="socket">Сокет</param>
        /// <param name="buffer">Буффер</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Задача чтения</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ValueTask ReadExactlyAsync(this Socket socket, Memory<byte> buffer, CancellationToken cancellationToken = default)
        {
            return socket.ReadExactlyAsync(buffer, buffer.Length, cancellationToken);
        }

        /// <summary>
        ///     Асинхронно прочитать гарантированное кол-во байт
        /// </summary>
        /// <param name="socket">Сокет</param>
        /// <param name="buffer">Буффер</param>
        /// <param name="count">Кол-во байт для чтения</param>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns>Задача чтения</returns>
        public static async ValueTask ReadExactlyAsync(this Socket socket, Memory<byte> buffer, int count, CancellationToken cancellationToken = default)
        {
            ArgumentNullException.ThrowIfNull(socket);

            var offset = 0;
            var currentOffset = offset;
            while (currentOffset < count)
            {
                var read = await socket.ReceiveAsync(buffer, SocketFlags.None, cancellationToken);

                if (read is 0)
                    throw new EndOfStreamException();

                currentOffset += read;
            }
        }
    }
}
