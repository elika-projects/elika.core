﻿namespace Elika;

/// <summary>
///     Результат операции
/// </summary>
/// <typeparam name="TError">Тип ошибки</typeparam>
public class OperationResult<TError>
{
    /// <summary>
    ///     Успешно выполнения операция
    /// </summary>
    public static readonly OperationResult<TError> SuccessResult = new();

    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="OperationResult{TError}" />
    /// </summary>
    protected OperationResult()
    {
        IsSuccess = true;
    }

    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="OperationResult{TError}" /> с указанием ошибки
    /// </summary>
    /// <param name="error">Ошибка</param>
    private OperationResult(TError error)
    {
        IsSuccess = false;
        Error = error;
    }

    /// <summary>
    ///     Признак успешного выполнения
    /// </summary>
    public bool IsSuccess { get; protected set; }

    /// <summary>
    ///     Ошибка
    /// </summary>
    public TError Error { get; protected set; }

    /// <summary>
    ///     Получить ошибочный результат операции с указанной ошибкой
    /// </summary>
    /// <param name="error">Ошибка</param>
    /// <returns>Результат операции</returns>
    public static OperationResult<TError> Failure(TError error)
    {
        return new OperationResult<TError>(error);
    }

    /// <summary>
    ///     Определение оператора который позволяет возвращать ошибку без оборачивания в <see cref="OperationResult{TError}" />
    /// </summary>
    /// <param name="value">Ошибка</param>
    public static implicit operator OperationResult<TError>(TError value)
    {
        return new OperationResult<TError>(value);
    }
}

/// <summary>
///     Результат операции с результатом
/// </summary>
/// <typeparam name="TError">Тип ошибки</typeparam>
/// <typeparam name="TResult">Тип полезной нагрузки</typeparam>
public class OperationResult<TResult, TError> : OperationResult<TError>
{
    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="OperationResult{TResult, TError}" /> с ошибкой
    /// </summary>
    /// <param name="error">Ошибка</param>
    private OperationResult(TError error)
    {
        IsSuccess = false;
        Error = error;
    }

    /// <summary>
    ///     Инициализирует новый экземпляр класса <see cref="OperationResult{TResult, TError}" /> с результатом
    /// </summary>
    /// <param name="result">Результат</param>
    private OperationResult(TResult result)
    {
        IsSuccess = true;
        Result = result;
    }

    /// <summary>
    ///     Полезная нагрузка
    /// </summary>
    public TResult Result { get; }

    /// <summary>
    ///     Получить успешный результат операции с результатом
    /// </summary>
    /// <param name="result">Результат</param>
    /// <returns>Результат операции</returns>
    public static OperationResult<TResult, TError> Success(TResult result)
    {
        return new OperationResult<TResult, TError>(result);
    }

    /// <summary>
    ///     Определение оператора который позволяет возвращать ошибку без оборачивания в
    ///     <see cref="OperationResult{TResult, TError}" />
    /// </summary>
    /// <param name="value">Ошибка</param>
    public static implicit operator OperationResult<TResult, TError>(TError value)
    {
        return new OperationResult<TResult, TError>(value);
    }

    /// <summary>
    ///     Определение оператора который позволяет возвращать результат без оборачивания в
    ///     <see cref="OperationResult{TResult, TError}" />
    /// </summary>
    /// <param name="value">Результат</param>
    public static implicit operator OperationResult<TResult, TError>(TResult value)
    {
        return new OperationResult<TResult, TError>(value);
    }
}