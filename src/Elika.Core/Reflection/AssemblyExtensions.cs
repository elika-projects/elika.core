﻿using System;
using System.Linq;
using System.Reflection;

namespace Elika.Reflection;

/// <summary>
///     Расширения для <see cref="Assembly" />
/// </summary>
public static class AssemblyExtensions
{
    /// <summary>
    ///     Поиск типов в сборке наследуемых от базового универсального типа <typeparamref name="T" />
    /// </summary>
    /// <typeparam name="T">Сборка</typeparam>
    /// <param name="assembly">Наследуемый тип</param>
    /// <returns>Типы</returns>
    public static Type[] FindByParentOf<T>(this Assembly assembly)
    {
        return assembly.FindByParentOf(typeof(T));
    }

    /// <summary>
    ///     Поиск типов в сборке наследуемых от базового универсального типа <paramref name="parent" />
    /// </summary>
    /// <param name="assembly">Сборка</param>
    /// <param name="parent">Базовый универсальный тип</param>
    /// <returns>Типы</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static Type[] FindByParentOf(this Assembly assembly, Type parent)
    {
        if (assembly == null)
            throw new ArgumentNullException(nameof(assembly));

        if (parent == null)
            throw new ArgumentNullException(nameof(parent));

        var result = assembly.GetTypes().Where(type => type.IsParentOf(parent)).ToArray();
        return result;
    }
}