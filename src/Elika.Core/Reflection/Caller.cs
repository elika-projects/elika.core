﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Elika.Reflection
{
    /// <summary>
    ///     Получение информации 
    /// </summary>
    public static class Caller
    {
        /// <summary>
        ///     Получить вызвавший метод
        /// </summary>
        /// <param name="level">Дополнительный уровень поднятия по стеку</param>
        /// <returns>Метод</returns>
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public static MemberInfo? GetCallerMethodInfo(int level = 0)
        {
            var frame = GetCallerStackFrame(level + 1);
            return frame?.GetMethod();
        }

        /// <summary>
        ///     Получить фрейм стека вызвавшего метода
        /// </summary>
        /// <param name="level">Дополнительный уровень поднятия по стеку</param>
        /// <returns>Фрейм стека</returns>
        public static StackFrame? GetCallerStackFrame(int level = 0)
        {
            var stackTrace = new StackTrace();
            return stackTrace.GetFrame(level + 1);
        }
    }
}
