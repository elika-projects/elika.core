﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Elika.Exceptions;

namespace Elika.Reflection;

/// <summary>
///     Расширения для <exception cref="MemberInfo"></exception>
/// </summary>
public static class MemberInfoExtensions
{
    /// <summary>
    ///     Получить атрибут типа
    /// </summary>
    /// <typeparam name="T">Тип атрибута</typeparam>
    /// <param name="member">Метаданные</param>
    /// <param name="inherit">Признак поиска в родителях</param>
    /// <returns>Атрибут</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static T GetCustomAttribute<T>(this MemberInfo member, bool inherit = false) where T : Attribute
    {
        return (T) member.GetCustomAttribute(typeof(T), inherit);
    }

    /// <summary>
    ///     Получить атрибут типа
    /// </summary>
    /// <param name="member">Метаданные</param>
    /// <param name="attributeType">Тип атрибута</param>
    /// <param name="inherit">Признак поиска в родителях</param>
    /// <returns>Атрибут</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="AttributeTypeRequiredException"></exception>
    public static object GetCustomAttribute(this MemberInfo member, Type attributeType, bool inherit = false)
    {
        if (member == null)
            throw new ArgumentNullException(nameof(member));

        if (attributeType == null)
            throw new ArgumentNullException(nameof(attributeType));

        if (!attributeType.IsParentOf(typeof(Attribute)))
            throw new AttributeTypeRequiredException(nameof(attributeType));

        var attrObj = member.GetCustomAttributes(attributeType, inherit).FirstOrDefault();
        return attrObj;
    }

    /// <summary>
    ///     Получить атрибуты типа
    /// </summary>
    /// <typeparam name="T">Тип атрибута</typeparam>
    /// <param name="member">Метаданные</param>
    /// <param name="inherit">Признак поиска в родителях</param>
    /// <returns>Атрибуты</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static T[] GetCustomAttributes<T>(this MemberInfo member, bool inherit = false) where T : Attribute
    {
        return member.GetCustomAttributes(typeof(T), inherit).OfType<T>().ToArray();
    }

    /// <summary>
    ///     Получить описание заданное через <see cref="DescriptionAttribute" />
    /// </summary>
    /// <param name="member">Метаданные</param>
    /// <param name="inherit">Признак поиска в родителях</param>
    /// <returns>Описание</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string GetDescription(this MemberInfo member, bool inherit = false)
    {
        return member.GetCustomAttribute<DescriptionAttribute>()?.Description;
    }
}