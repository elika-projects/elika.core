﻿using System;
using System.Runtime.InteropServices;

namespace Elika.Runtime.InteropServices;

/// <summary>
///     Инструменты связанные с <see cref="Marshal" />
/// </summary>
public class MarshalTools
{
    /// <summary>
    ///     Привести массив байт к структуре
    /// </summary>
    /// <typeparam name="T">Тип структуры</typeparam>
    /// <param name="buffer">Буффер байтов</param>
    /// <returns>Структура</returns>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
    public static object BytesToStructure<T>(byte[] buffer)
    {
        return (T) BytesToStructure(buffer, typeof(T));
    }

    /// <summary>
    ///     Привести массив байт к структуре
    /// </summary>
    /// <param name="buffer">Тип структуры</param>
    /// <param name="dataType">Структура</param>
    /// <returns>Структура</returns>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
    public static object BytesToStructure(byte[] buffer, Type dataType)
    {
        if (buffer == null)
            throw new ArgumentNullException(nameof(buffer));

        if (dataType == null)
            throw new ArgumentNullException(nameof(dataType));

        var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);

        try
        {
            return Marshal.PtrToStructure(gch.AddrOfPinnedObject(), dataType);
        }
        finally
        {
            gch.Free();
        }
    }

    /// <summary>
    ///     Привести структуру к массиву байт
    /// </summary>
    /// <typeparam name="T">Тип структуры</typeparam>
    /// <param name="obj">Структура</param>
    /// <returns>Массив байт</returns>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
    public static byte[] StructureToBytes<T>(T obj) where T : struct
    {
        return StructureToBytes((object) obj);
    }

    /// <summary>
    ///     Привести структуру к массиву байт
    /// </summary>
    /// <param name="obj">Структура</param>
    /// <returns>Массив байт</returns>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
    public static byte[] StructureToBytes(object obj)
    {
        if (obj == null)
            throw new ArgumentNullException(nameof(obj));

        var size = Marshal.SizeOf(obj);
        var buffer = new byte[size];
        var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);

        try
        {
            Marshal.StructureToPtr(obj, gch.AddrOfPinnedObject(), false);
            return buffer;
        }
        finally
        {
            gch.Free();
        }
    }
}