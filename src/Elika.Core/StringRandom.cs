﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Elika;

/// <summary>
///     Генератор случайных строк
/// </summary>
public class StringRandom
{
    /// <summary>
    ///     Базовый набор знаков
    /// </summary>
    public static readonly string Alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    /// <summary>
    ///     Базовая длинна строки
    /// </summary>
    public static readonly int DefaultSize = 64;

    /// <summary>
    ///     Геренация случайной строки c использованием <see cref="RNGCryptoServiceProvider" />
    /// </summary>
    /// <param name="alphanumeric">Массив знаков для генерации</param>
    /// <param name="count">Кол-во символом в строке</param>
    /// <returns>Сгенерированная строка</returns>
    public static string SecureGenerate(string alphanumeric, int count)
    {
        ArgumentNullException.ThrowIfNull(alphanumeric);

        var indexes = GetSecureRandomIndex(alphanumeric.Length, count);
        var sb = new StringBuilder();
        for (var i = 0; i < count; i++)
            sb.Append(alphanumeric[indexes[i]]);

        return sb.ToString();
    }

    /// <summary>
    ///     Геренация случайной строки c использованием <see cref="RNGCryptoServiceProvider" /> на основе
    ///     <see cref="Alphanumeric" />
    /// </summary>
    /// <param name="count">Кол-во символом в строке</param>
    /// <returns>Сгенерированная строка</returns>
    public static string SecureGenerate(int count)
    {
        return SecureGenerate(Alphanumeric, count);
    }

    /// <summary>
    ///     Геренация случайной строки c использованием <see cref="RNGCryptoServiceProvider" /> на основе
    ///     <see cref="Alphanumeric" /> длинной <see cref="DefaultSize" />
    /// </summary>
    /// <returns>Сгенерированная строка</returns>
    public static string SecureGenerate()
    {
        return SecureGenerate(Alphanumeric, DefaultSize);
    }

    private static int[] GetSecureRandomIndex(int maxValue, int count)
    {
        var rnd = new RNGCryptoServiceProvider();
        var bytes = new byte[count * 4];
        rnd.GetBytes(bytes);
        var res = new int[count];
        for (var i = 0; i < count; i++)
        {
            var val = BitConverter.ToUInt32(bytes, i * 4);
            var index = val % maxValue;
            res[i] = (int) index;
        }

        rnd.Dispose();
        return res;
    }
}