﻿using System;

namespace Elika;

/// <summary>
///     Время формате Unix
/// </summary>
public struct UnixTime
{
    private static readonly DateTime TimeStamp = new(1970, 1, 1);

    private DateTime _dateTime = DateTime.MinValue;

    public UnixTime()
    {
    }

    public override string ToString()
    {
        return _dateTime.ToString();
    }

    public string ToString(string? format)
    {
        return _dateTime.ToString(format);
    }

    public string ToString(IFormatProvider provider)
    {
        return _dateTime.ToString(provider);
    }

    /// <summary>
    ///     Получить кол-во секунд в формате Unix
    /// </summary>
    /// <param name="dateTime">Текущее время</param>
    /// <returns>Кол-во тиков в формате Unix</returns>
    public static int ToUnixTimeSeconds(DateTime dateTime)
    {
        return (int) (dateTime - TimeStamp).TotalSeconds;
    }

    /// <summary>
    ///     Получить <see cref="DateTime" /> из секунд в формате Unix
    /// </summary>
    /// <param name="unixTimeSeconds">Секунды в формате Unix</param>
    /// <returns>Время</returns>
    public static DateTime ToDateTime(int unixTimeSeconds)
    {
        return TimeStamp.AddSeconds(unixTimeSeconds);
    }

    /// <summary>
    ///     Получить <see cref="DateTime" /> из секунд в формате Unix
    /// </summary>
    /// <param name="unixTimeSeconds">Секунды в формате Unix</param>
    /// <returns>Время</returns>
    public static DateTime ToDateTime(uint unixTimeSeconds)
    {
        return ToDateTime((int) unixTimeSeconds);
    }
}