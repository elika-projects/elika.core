﻿namespace Elika.Utilities.Plugin;

/// <summary>
///     Базовый плагин
/// </summary>
public interface IPlugin
{
    /// <summary>
    ///     Событие инициализации плагина
    /// </summary>
    /// <param name="pluginManager">Менеджер плагинов</param>
    void OnInit(PluginManager pluginManager);

    /// <summary>
    ///     Событие выгрузки плагина
    /// </summary>
    void OnUnload();
}