﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Elika.Utilities.Plugin;

/// <summary>
///     Базовая реализация менеджера плагинов
/// </summary>
public class PluginManager : PluginManager<IPlugin>
{
    /// <inheritdoc />
    protected override void SetUp(IPlugin plugin)
    {
        if (plugin == null)
            throw new ArgumentNullException(nameof(plugin));

        plugin.OnInit(this);
    }

    /// <inheritdoc />
    protected override void Unload(IPlugin plugin)
    {
        if (plugin == null)
            throw new ArgumentNullException(nameof(plugin));

        plugin.OnUnload();
    }
}

/// <summary>
///     Базовая реализация менеджера плагинов
/// </summary>
/// <typeparam name="TPlugin">Тип плагина</typeparam>
public class PluginManager<TPlugin>
{
    private readonly object _locker = new();

    private readonly Dictionary<Type, TPlugin> _plugins = new();
    private readonly Type _type = typeof(TPlugin);

    /// <summary>
    ///     Загруженные плагины
    /// </summary>
    public TPlugin[] Plugins => GetPlugins();

    /// <summary>
    ///     Получить загруженные плагины
    /// </summary>
    /// <returns></returns>
    public TPlugin[] GetPlugins()
    {
        lock (_locker)
        {
            return _plugins.Values.ToArray();
        }
    }

    /// <summary>
    ///     Загрузить сборку с плагинами из файла
    /// </summary>
    /// <param name="fileName">Путь до файла</param>
    public void LoadPluginsFromFile(string fileName)
    {
        if (fileName == null)
            throw new ArgumentNullException(nameof(fileName));

        var bytes = File.ReadAllBytes(fileName);
        var assembly = Assembly.Load(bytes);
        LoadPluginsFromAssembly(assembly);
    }

    /// <summary>
    ///     Загрузить плагины из сборки
    /// </summary>
    /// <param name="assembly">Сборка</param>
    public void LoadPluginsFromAssembly(Assembly assembly)
    {
        if (assembly == null)
            throw new ArgumentNullException(nameof(assembly));

        var types = assembly.GetTypes().Where(type => type.IsParentOf(_type)).ToArray();
    }

    /// <summary>
    ///     Загрузить плагин из типа
    /// </summary>
    /// <param name="pluginType">Тип плагина</param>
    public void LoadPlugin(Type pluginType)
    {
        if (pluginType == null)
            throw new ArgumentNullException(nameof(pluginType));

        if (!pluginType.IsParentOf(_type))
            throw new Exception(); // TODO

        lock (_locker)
        {
            if (_plugins.ContainsKey(pluginType))
                throw new Exception(); // TODO

            var pluginObj = ResolveType(pluginType);

            if (pluginObj == null)
                throw new Exception(); // TODO

            if (!(pluginObj is TPlugin plugin))
                throw new Exception(); // TODO

            SetUp(plugin);
            _plugins.Add(pluginType, plugin);
        }
    }

    /// <summary>
    ///     Вызгрузить плагин
    /// </summary>
    /// <param name="plugin">Плагин</param>
    public void UnloadPlugin(TPlugin plugin)
    {
        if (plugin == null)
            throw new ArgumentNullException(nameof(plugin));

        lock (_locker)
        {
            var type = plugin.GetType();
            if (!_plugins.ContainsKey(type))
                throw new Exception(); // TODO

            Unload(plugin);
            _plugins.Remove(type);
        }
    }

    /// <summary>
    ///     Разрешить зависимости плагина
    /// </summary>
    /// <param name="pluginType">Тип плагина</param>
    /// <returns>Плагин</returns>
    protected virtual object ResolveType(Type pluginType)
    {
        if (pluginType == null)
            throw new ArgumentNullException(nameof(pluginType));

        var plugin = Activator.CreateInstance(pluginType);
        return plugin;
    }

    /// <summary>
    ///     Выгрузить плагин
    /// </summary>
    /// <param name="plugin">Плагин</param>
    protected virtual void Unload(TPlugin plugin)
    {
    }

    /// <summary>
    ///     Загрузить плагин
    /// </summary>
    /// <param name="plugin">Плагин</param>
    protected virtual void SetUp(TPlugin plugin)
    {
    }
}