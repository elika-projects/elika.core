﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elika;

/// <summary>
///     Асинхронное событие
/// </summary>
/// <typeparam name="TEventArgs">Тип аргумента события</typeparam>
public class AsyncEvent<TEventArgs> where TEventArgs : EventArgs
{
    private readonly List<Func<object, TEventArgs, Task>> _invocations = new();

    private readonly object _locker = new();

    internal async Task InvokeAsync(object sender, TEventArgs eventArgs)
    {
        List<Func<object, TEventArgs, Task>> currentInvocations;

        lock (_locker)
        {
            currentInvocations = _invocations.ToList();
        }

        foreach (var callback in currentInvocations)
            await callback(sender, eventArgs);
    }

    /// <summary>
    ///     Подписаться на событие
    /// </summary>
    /// <param name="e">Текущая сборщик подписей на событие</param>
    /// <param name="callback">Подпись на событие</param>
    /// <returns>Текущий сборшик подписей на событие</returns>
    /// <exception cref="NullReferenceException"></exception>
    public static AsyncEvent<TEventArgs> operator +(
        AsyncEvent<TEventArgs> e, Func<object, TEventArgs, Task> callback)
    {
        ArgumentNullException.ThrowIfNull(callback);

        e ??= new AsyncEvent<TEventArgs>();

        lock (e._locker)
        {
            e._invocations.Add(callback);
        }

        return e;
    }

    /// <summary>
    ///     Отписаться на событие
    /// </summary>
    /// <param name="e">Текущая сборщик подписей на событие</param>
    /// <param name="callback">Подпись на событие</param>
    /// <returns>Текущий сборшик подписей на событие</returns>
    /// <exception cref="NullReferenceException"></exception>
    public static AsyncEvent<TEventArgs> operator -(
        AsyncEvent<TEventArgs> e, Func<object, TEventArgs, Task> callback)
    {
        ArgumentNullException.ThrowIfNull(callback);

        if (e == null)
            e = new AsyncEvent<TEventArgs>();
        else
            lock (e._locker)
            {
                e._invocations.Remove(callback);
            }

        return e;
    }
}