﻿using System;
using System.Threading.Tasks;

namespace Elika;

/// <summary>
///     Расширения для <see cref="AsyncEvent{TEventArgs}" />
/// </summary>
public static class AsyncEventExtensions
{
    /// <summary>
    ///     Вызов асинхронного события
    /// </summary>
    /// <typeparam name="TEventArgs">Тип аргумента события</typeparam>
    /// <param name="asyncEvent">Сборщик подписей на событие</param>
    /// <param name="sender">Отправитель события</param>
    /// <param name="args">Аргументы</param>
    /// <returns>Задача на асинхронный вызов события</returns>
    public static Task InvokeAsync<TEventArgs>(this AsyncEvent<TEventArgs> asyncEvent, object sender,
        TEventArgs args) where TEventArgs : EventArgs
    {
        ArgumentNullException.ThrowIfNull(asyncEvent);
        ArgumentNullException.ThrowIfNull(sender);
        ArgumentNullException.ThrowIfNull(args);

        return asyncEvent.InvokeAsync(sender, args);
    }
}