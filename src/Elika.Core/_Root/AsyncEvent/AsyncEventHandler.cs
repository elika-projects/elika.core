﻿using System;
using System.Threading.Tasks;

namespace Elika;

/// <summary>
///     Обработчик асинхронного метода
/// </summary>
/// <param name="sender">Вызвовший событие</param>
/// <param name="e">Аргументы</param>
/// <returns>Задача</returns>
public delegate Task AsyncEventHandler(object sender, EventArgs e);

/// <summary>
///     Обработчик асинхронного метода
/// </summary>
/// <typeparam name="TEventArgs">Тип аргументов</typeparam>
/// <param name="sender">Вызвовший событие</param>
/// <param name="e">Аргументы</param>
/// <returns>Задача</returns>
public delegate Task AsyncEventHandler<in TEventArgs>(object sender, TEventArgs e);