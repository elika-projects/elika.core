﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Elika;

/// <summary>
///     Расширения для <see cref="AsyncEventHandler" /> и <see cref="AsyncEventHandler{TEventArgs}" />
/// </summary>
public static class AsyncEventHandlerExtensions
{
    /// <summary>
    ///     Асинхронный вызов события
    /// </summary>
    /// <param name="eventHandler">Обработчик события</param>
    /// <param name="sender">Вызвовший событие</param>
    /// <param name="e">Аргументы</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача асинхронного вызова</returns>
    public static async Task InvokeAsync(this AsyncEventHandler eventHandler, object sender, EventArgs e,
        CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(eventHandler);
        ArgumentNullException.ThrowIfNull(sender);
        ArgumentNullException.ThrowIfNull(e);

        var invocations = eventHandler.GetInvocationList();

        foreach (var invocation in invocations)
        {
            var asyncDelegate = (AsyncEventHandler)invocation;
            await asyncDelegate.Invoke(sender, e);
        }
    }

    /// <summary>
    ///     Асинхронный вызов события
    /// </summary>
    /// <typeparam name="TEventArgs">Тип аргументов</typeparam>
    /// <param name="eventHandler">Обработчик события</param>
    /// <param name="sender">Вызвовший событие</param>
    /// <param name="e">Аргументы</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача асинхронного вызова</returns>
    public static async Task InvokeAsync<TEventArgs>(this AsyncEventHandler<TEventArgs> eventHandler, object sender,
        TEventArgs e, CancellationToken cancellationToken = default)
    {
        if (eventHandler == null)
            return;

        var invocations = eventHandler.GetInvocationList();

        foreach (var invocation in invocations)
        {
            var asyncDelegate = (AsyncEventHandler<TEventArgs>)invocation;
            await asyncDelegate.Invoke(sender, e);
        }
    }
}