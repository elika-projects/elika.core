﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Elika;

/// <summary>
///     Асинхронная блокировка
/// </summary>
public class AsyncLock : ObjectDisposable
{
    private static readonly ConcurrentDictionary<object, AsyncLockData> AsyncData = new();

    private readonly AsyncLockData _data;

    private readonly object _locker;

    private int? _lockId;

    private AsyncLock(object locker)
    {
        _locker = locker;
        _data = GetData(locker);
    }

    private int GetLockId()
    {
        return _lockId ??= _locker.GetHashCode();
    }

    /// <summary>
    ///     Асинхронно ожидает свобождения блокировки если есть
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача ожидания с текущим блокировщиком</returns>
    public async Task<IDisposable> WaitAsync(CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();

        _data.IncrementCount();

        Debug.WriteLine("Enter AsyncLock(ID {0}), in queue {1} locks", GetLockId(), _data.Count);
        await _data.Semaphore.WaitAsync(cancellationToken);
        cancellationToken.ThrowIfCancellationRequested();
        return this;
    }

    /// <summary>
    ///     Освободить блокировку
    /// </summary>
    private void Release()
    {
        _data.DecrementCount();
        Debug.WriteLine("Release AsyncLock(ID {0}), in queue {1} locks", GetLockId(), _data.Count);
        var isRemoved = TryRemove(_locker);

        Debug.WriteLineIf(isRemoved, $"AsyncLock(ID {GetLockId()}) cleanup");

        _data.Semaphore.Release();
    }

    /// <inheritdoc />
    protected override void Dispose(bool disposing)
    {
        Release();
    }

    private static AsyncLockData GetData(object locker)
    {
        return AsyncData.GetOrAdd(locker, k => new AsyncLockData());
    }

    private static bool TryRemove(object locker)
    {
        if (!AsyncData.TryGetValue(locker, out var data))
            return false;

        return data.Count == 0 && AsyncData.TryRemove(locker, out _);
    }

    /// <summary>
    ///     Асинхронная блокировка
    /// </summary>
    /// <param name="locker">Блокировщик</param>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <returns>Задача асинхронной блокировки</returns>
    public static Task<IDisposable> LockAsync(object locker, CancellationToken cancellationToken = default)
    {
        var asyncLock = new AsyncLock(locker);
        return asyncLock.WaitAsync(cancellationToken);
    }
}