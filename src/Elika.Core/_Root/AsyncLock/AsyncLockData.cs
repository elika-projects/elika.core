﻿using System.Threading;

namespace Elika;

/// <summary>
///     Данные асинхронной блокировки
/// </summary>
internal class AsyncLockData
{
    private int _count;

    /// <summary>
    ///     Семафор блокировки
    /// </summary>
    public SemaphoreSlim Semaphore { get; } = new(1, 1);

    /// <summary>
    ///     Кол-во блокировок в очереди
    /// </summary>
    public int Count => _count;

    public void IncrementCount()
    {
        Interlocked.Increment(ref _count);
    }

    public void DecrementCount()
    {
        Interlocked.Decrement(ref _count);
    }
}