﻿using System.Threading.Tasks;

namespace Elika;

/// <summary>
///     Освобождение ресурсов объекта асинхронно
/// </summary>
public abstract class AsyncObjectDisposable : ObjectDisposable, IAsyncObjectDisposable
{
    /// <inheritdoc />
    public virtual async ValueTask DisposeAsync()
    {
        IsDisposed = true;
        await DisposeAsync(true);
    }

    /// <summary>
    ///     Освободить ресурсы асинхронно
    /// </summary>
    /// <param name="disposing">Освободить управляемые ресурсы</param>
    protected virtual Task DisposeAsync(bool disposing)
    {
        return Task.CompletedTask;
    }
}