﻿using System;

namespace Elika;

/// <summary>
///     Реализация интерфейса освобождения ресурсов объекта
/// </summary>
public interface IAsyncObjectDisposable : IAsyncDisposable
{
    /// <summary>
    ///     Признак освобождения ресурсов
    /// </summary>
    bool IsDisposed { get; }
}