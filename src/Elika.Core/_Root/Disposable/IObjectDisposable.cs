﻿using System;

namespace Elika;

/// <summary>
///     Реализация интерфейса освобождения ресурсов объекта
/// </summary>
public interface IObjectDisposable : IDisposable
{
    /// <summary>
    ///     Признак освобождения ресурсов
    /// </summary>
    bool IsDisposed { get; }
}