﻿using System;

namespace Elika;

/// <summary>
///     Освобождение ресурсов объекта
/// </summary>
public abstract class ObjectDisposable : IObjectDisposable
{
    /// <inheritdoc />
    public bool IsDisposed { get; protected internal set; }

    /// <inheritdoc />
    public virtual void Dispose()
    {
        Dispose(!IsDisposed);
        GC.SuppressFinalize(this);
        IsDisposed = true;
    }

    /// <summary>
    ///     Освободить ресурсы
    /// </summary>
    /// <param name="disposing">Освободить управляемые ресурсы</param>
    protected virtual void Dispose(bool disposing)
    {
    }
}