﻿using System;

namespace Elika;

/// <summary>
///     Расширения для массивов
/// </summary>
public static class ArrayExtensions
{
    /// <summary>
    ///     Поменять местами элементы массива
    /// </summary>
    /// <typeparam name="T">Тип данных массива</typeparam>
    /// <param name="array">Массив</param>
    /// <param name="index1">Индекс 1го элемента</param>
    /// <param name="index2">Индекс 2го элемента</param>
    public static void SwapAt<T>(this T[] array, int index1, int index2)
    {
        if (array == null)
            throw new ArgumentNullException(nameof(array));

        (array[index1], array[index2]) = (array[index2], array[index1]);
    }
}