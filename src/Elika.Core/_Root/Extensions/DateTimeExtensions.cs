﻿using System;

namespace Elika;

/// <summary>
///     Расширения для <see cref="DateTime" />
/// </summary>
public static class DateTimeExtensions
{
    /// <summary>
    ///     Получить кол-во секунд в формате Unix
    /// </summary>
    /// <param name="dateTime">Текущее время</param>
    /// <returns>Кол-во секунд в формате Unix</returns>
    public static int ToUnixTimeSeconds(this DateTime dateTime)
    {
        return UnixTime.ToUnixTimeSeconds(dateTime);
    }
}