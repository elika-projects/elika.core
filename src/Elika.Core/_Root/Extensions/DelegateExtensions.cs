﻿using System;
using System.ComponentModel;
using Elika.Reflection;

namespace Elika;

/// <summary>
///     Расширения для <see cref="DelegateExtensions" />
/// </summary>
public static class DelegateExtensions
{
    /// <summary>
    ///     Получить описание делегата
    /// </summary>
    /// <param name="del">Делегат</param>
    /// <returns>Описание</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string GetDescription(this Delegate del)
    {
        ArgumentNullException.ThrowIfNull(del);

        return del.Method.GetCustomAttribute<DescriptionAttribute>()?.Description;
    }
}