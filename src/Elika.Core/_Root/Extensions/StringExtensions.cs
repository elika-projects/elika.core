﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elika;

/// <summary>
///     Расширения для <see cref="string" />
/// </summary>
public static class StringExtensions
{
    /// <summary>
    ///     Проверка строки на NULL или на пустое значение
    /// </summary>
    /// <param name="text">Исходная строка</param>
    /// <returns>Признак наличия NULL или пустой строки</returns>
    public static bool IsNullOrEmpty(this string text)
    {
        return string.IsNullOrEmpty(text);
    }

    /// <summary>
    ///     Проверка строки на NULL
    /// </summary>
    /// <param name="text">Исходная стркоа</param>
    /// <returns>Признак наличия NULL</returns>
    public static bool IsNull(this string? text)
    {
        return text is null;
    }

    /// <summary>
    ///     Проверка строка ни пустое значение
    /// </summary>
    /// <param name="text">Исходная строка</param>
    /// <returns>Признак наличия пустого значения</returns>
    public static bool IsEmpty(this string text)
    {
        return text.Equals(string.Empty);
    }

    /// <summary>
    ///     Поднять регистр у первой буквы в строке
    /// </summary>
    /// <param name="text">Исходная строка</param>
    /// <returns>Исходная строка с поднятым регистром первого символа</returns>
    public static string ToUpperCaseFirstSymbol(this string text)
    {
        if (text.IsNullOrEmpty())
            return text;

        var sb = new StringBuilder();
        sb.Append(char.ToUpper(text[0]));

        if (text.Length > 1)
            sb.Append(text, 1, text.Length - 1);

        return sb.ToString();
    }

    /// <summary>
    ///     Объединение строк <paramref name="strings" /> используя разделитель <paramref name="separator" />
    /// </summary>
    /// <param name="strings">Строки</param>
    /// <param name="separator">Разделитель</param>
    /// <returns>Строка</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string JoinStrings(this IEnumerable<string> strings, string separator)
    {
        if (strings == null)
            throw new ArgumentNullException(nameof(strings));

        return string.Join(separator, strings);
    }

    /// <summary>
    ///     Проверка наличия символов в строке
    /// </summary>
    /// <param name="text">Исходная строка</param>
    /// <param name="chars">Символы</param>
    /// <returns>Признак наличия символов</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool IsCharsContains(this string text, IEnumerable<char> chars)
    {
        if (chars == null)
            throw new ArgumentNullException(nameof(chars));

        return text.IsCharsContains(chars.ToArray());
    }

    /// <summary>
    ///     Проверка наличия символов в строке
    /// </summary>
    /// <param name="text">Исходная строка</param>
    /// <param name="chars">Символы</param>
    /// <returns>Признак наличия символов</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool IsCharsContains(this string text, params char[] chars)
    {
        ArgumentNullException.ThrowIfNull(text);
        ArgumentNullException.ThrowIfNull(chars);

        return !text.IsNullOrEmpty() && text.Any(chars.Contains);
    }

    /// <summary>
    ///     Стабильный хеш строки (не зависим от платформы)
    /// </summary>
    /// <param name="text">Строка</param>
    /// <returns>Хэш строки</returns>
    public static int GetStableHashCode(this string text)
    {
        ArgumentNullException.ThrowIfNull(text);
        unchecked
        {
            var hash = 23;
            for(var index = 0; index < text.Length; index++)
                hash = hash * 31 + text[index];
            return hash;
        }
    }
}