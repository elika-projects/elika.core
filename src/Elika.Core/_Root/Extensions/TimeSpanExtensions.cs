﻿using System;

namespace Elika;

/// <summary>
///     Расширения для <see cref="TimeSpan" />
/// </summary>
public static class TimeSpanExtensions
{
    /// <summary>
    ///     Добавить дни во временной интервал
    /// </summary>
    /// <param name="timeSpan">Интервал</param>
    /// <param name="days">Кол-во дней</param>
    /// <returns>Интервал</returns>
    /// <exception cref="OverflowException"></exception>
    public static TimeSpan AddDays(this TimeSpan timeSpan, int days)
    {
        timeSpan = timeSpan.Add(new TimeSpan(days, 0, 0, 0));
        return timeSpan;
    }

    /// <summary>
    ///     Добавить часы во временной интервал
    /// </summary>
    /// <param name="timeSpan">Интервал</param>
    /// <param name="hours">Кол-во часов</param>
    /// <returns>Интервал</returns>
    /// <exception cref="OverflowException"></exception>
    public static TimeSpan AddHours(this TimeSpan timeSpan, int hours)
    {
        timeSpan = timeSpan.Add(new TimeSpan(0, hours, 0, 0));
        return timeSpan;
    }

    /// <summary>
    ///     Добавить минуты во временной интервал
    /// </summary>
    /// <param name="timeSpan">Интервал</param>
    /// <param name="minutes">Кол-во минут</param>
    /// <returns>Интервал</returns>
    /// <exception cref="OverflowException"></exception>
    public static TimeSpan AddMinutes(this TimeSpan timeSpan, int minutes)
    {
        timeSpan = timeSpan.Add(new TimeSpan(0, 0, minutes, 0));
        return timeSpan;
    }

    /// <summary>
    ///     Добавить секунды во временной интервал
    /// </summary>
    /// <param name="timeSpan">Интервал</param>
    /// <param name="seconds">Кол-во секунд</param>
    /// <returns>Интервал</returns>
    /// <exception cref="OverflowException"></exception>
    public static TimeSpan AddSeconds(this TimeSpan timeSpan, int seconds)
    {
        timeSpan = timeSpan.Add(new TimeSpan(0, 0, 0, seconds));
        return timeSpan;
    }

    /// <summary>
    ///     Добавить тики во временной интервал
    /// </summary>
    /// <param name="timeSpan">Интервал</param>
    /// <param name="ticks">Кол-во тиков</param>
    /// <returns>Интервал</returns>
    /// <exception cref="OverflowException"></exception>
    public static TimeSpan AddTicks(this TimeSpan timeSpan, long ticks)
    {
        timeSpan = timeSpan.Add(new TimeSpan(ticks));
        return timeSpan;
    }
}