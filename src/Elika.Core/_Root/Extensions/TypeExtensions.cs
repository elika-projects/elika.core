﻿using System;
using System.Linq;
using Elika.Exceptions;

namespace Elika;

/// <summary>
///     Расширения для <see cref="Type" />
/// </summary>
public static class TypeExtensions
{
    /// <summary>
    ///     Проверка, является ли тип <paramref name="parent" /> родительским для <paramref name="type" />
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <param name="parent">Родительский тип</param>
    /// <returns>Признак наличия</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool IsParentOf(this Type type, Type parent)
    {
        if (type == null)
            throw new ArgumentNullException(nameof(type));

        if (parent == null)
            throw new ArgumentNullException(nameof(parent));

        if (parent.IsClass)
            return parent.IsGenericTypeDefinition
                ? type.IsClassDefinitionOf(parent)
                : type.IsSubclassOf(parent);

        if (parent.IsInterface)
            return parent.IsGenericTypeDefinition
                ? type.IsInterfaceDefinitionOf(parent)
                : type.IsSubclassOf(parent);

        return false;
    }

    /// <summary>
    ///     Проверка, является ли тип <paramref name="type" /> производным от <typeparamref name="T" />
    /// </summary>
    /// <typeparam name="T">Тип интерфейса</typeparam>
    /// <param name="type">Тип интерфейса</param>
    /// <returns>Признак наличия</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InterfaceTypeRequiredException"></exception>
    public static bool IsSubinterfaceOf<T>(this Type type)
    {
        return type.IsSubinterfaceOf(typeof(T));
    }

    /// <summary>
    ///     Проверка, является ли тип <paramref name="type" /> производным от <paramref name="interfaceType" />
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <param name="interfaceType">Тип интерфейса</param>
    /// <returns>Признак наличия</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InterfaceTypeRequiredException"></exception>
    public static bool IsSubinterfaceOf(this Type type, Type interfaceType)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(interfaceType);

        if (!interfaceType.IsInterface)
            throw new InterfaceTypeRequiredException(nameof(interfaceType));

        return interfaceType.IsAssignableFrom(type);
    }

    /// <summary>
    ///     Получить интерфейс с типом <typeparamref name="T" />
    /// </summary>
    /// <typeparam name="T">Тип интерфейса</typeparam>
    /// <param name="type">Изначальний тип</param>
    /// <returns>Тип интерфейса</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InterfaceTypeRequiredException"></exception>
    public static Type GetInterface<T>(this Type type)
    {
        return type.GetInterface(typeof(T));
    }

    /// <summary>
    ///     Получить интерфейс с типом <paramref name="interfaceType" />
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <param name="interfaceType">Тип интерфейса</param>
    /// <returns>Тип интерфейса</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InterfaceTypeRequiredException"></exception>
    public static Type GetInterface(this Type type, Type interfaceType)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(interfaceType);

        if (!interfaceType.IsInterface)
            throw new InterfaceTypeRequiredException(nameof(interfaceType));

        return type.GetInterfaces().FirstOrDefault(i => i == interfaceType);
    }

    /// <summary>
    ///     Получить интерфейс определённый типом <paramref name="definitionType" />
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <param name="definitionType">Тип определения интерфейса</param>
    /// <returns>Тип интерфейса</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InterfaceTypeRequiredException"></exception>
    public static Type GetInterfaceByDefinition(this Type type, Type definitionType)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(definitionType);

        if (!definitionType.IsInterface)
            throw new InterfaceTypeRequiredException(nameof(definitionType));

        if (!definitionType.IsGenericTypeDefinition)
            throw new NonDefinitionTypeException(nameof(definitionType));

        return type.GetInterfaces().FirstOrDefault(i =>
            i.IsGenericType && !i.IsGenericTypeDefinition && i.GetGenericTypeDefinition() == definitionType);
    }

    /// <summary>
    ///     Проверка, является ли тип <paramref name="type" /> определённым от универсального типа
    ///     <paramref name="definitionType" />
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <param name="definitionType">Универсальный тип</param>
    /// <returns>Признак наличия</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="NonDefinitionTypeException"></exception>
    /// <exception cref="TypeNotSupportedException"></exception>
    public static bool IsDefinitionOf(this Type type, Type definitionType)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(definitionType);

        if (definitionType.IsGenericType)
            throw new NonDefinitionTypeException(nameof(definitionType));

        if (definitionType.IsClass)
            return type.IsClassDefinitionOf(definitionType);

        if (definitionType.IsInterface)
            return type.IsInterfaceDefinitionOf(definitionType);

        throw new TypeNotSupportedException(nameof(definitionType));
    }

    /// <summary>
    ///     Проверка, является ли тип <paramref name="type" /> определённым от универсального класса
    ///     <paramref name="definition" />
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <param name="definition">Универсальный тип</param>
    /// <returns>Признак наличия</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="NonDefinitionTypeException"></exception>
    /// <exception cref="ClassTypeRequiredException"></exception>
    public static bool IsClassDefinitionOf(this Type type, Type definition)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(definition);

        if (!definition.IsGenericType)
            throw new NonDefinitionTypeException(nameof(definition));

        if (!definition.IsClass)
            throw new ClassTypeRequiredException(nameof(definition));

        if (!definition.IsGenericTypeDefinition)
            throw new NonDefinitionTypeException(nameof(definition));

        if (type.IsGenericType && type.GetGenericTypeDefinition() == definition)
            return true;

        if (type.BaseType == null)
            return false;

        return type.BaseType.IsClassDefinitionOf(definition);
    }

    /// <summary>
    ///     Проверка, является ли тип <paramref name="type" /> определённым от универсального класса
    ///     <paramref name="definition" />
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <param name="definition">Универсальный тип</param>
    /// <returns>Признак наличия</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="NonDefinitionTypeException"></exception>
    /// <exception cref="InterfaceTypeRequiredException"></exception>
    public static bool IsInterfaceDefinitionOf(this Type type, Type definition)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(definition);

        if (!definition.IsInterface)
            throw new InterfaceTypeRequiredException(nameof(definition));

        if (!definition.IsGenericTypeDefinition)
            throw new NonDefinitionTypeException(nameof(definition));

        if (type.IsGenericType && type.GetGenericTypeDefinition() == definition)
            return true;

        if (type.BaseType == null)
            return false;

        var interfaces = type.GetInterfaces();
        if (!interfaces.Any())
            return false;

        foreach (var @interface in interfaces)
        {
            if (!@interface.IsGenericType && @interface.IsInterfaceDefinitionOf(definition))
                return true;

            if (@interface.IsGenericType && @interface.GetGenericTypeDefinition() == definition)
                return true;
        }

        return false;
    }

    /// <summary>
    ///     Проверка, является ли тип структурой
    /// </summary>
    /// <param name="type">Изначальний тип</param>
    /// <returns>Признак структуры</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool IsStruct(this Type type)
    {
        ArgumentNullException.ThrowIfNull(type);

        return type.IsValueType && !type.IsEnum;
    }

    /// <summary>
    ///     Проверить схожесть типов
    /// </summary>
    /// <param name="type">1й тип</param>
    /// <param name="otherType">2й тип</param>
    /// <returns>Признак схожести</returns>
    public static bool IsSomeAs(this Type type, Type otherType)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(otherType);

        return type == otherType || type.IsParentOf(otherType) || otherType.IsParentOf(type);
    }

    /// <summary>
    ///     Проверка, является ли тип примитивом
    /// </summary>
    /// <param name="type">Тип</param>
    /// <returns>Признак примитива</returns>
    public static bool IsPrimitive(this Type type)
    {
        ArgumentNullException.ThrowIfNull(type);

        return type == typeof(string) || (type.IsValueType && type.IsPrimitive);
    }
}