﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Elika.Core.Tests;

public class AsyncLockTests
{
    private int _inOneThreadAndOneLockerInWorkCounter;

    private int taskCompleted;

    [OneTimeSetUp]
    public void StartTest()
    {
        Trace.Listeners.Add(new ConsoleTraceListener());
    }

    [Test]
    public void InOneThreadAndOneLocker()
    {
        var locker = new LockClass();
        Test(locker);
    }

    private void Test(object locker, int count = 100)
    {
        var tasks = new Task[count];

        for (var i = 0; i < count; i++)
            tasks[i] = DemoTask(i, locker);

        Task.WaitAll(tasks);
    }

    [Test]
    public void InMoreThreadAndOneLocker()
    {
        var locker = new LockClass();
        var thread1 = new Thread(() => Test(locker, 20));
        var thread2 = new Thread(() => Test(locker, 20));
        thread1.Start();
        thread2.Start();

        SpinWait.SpinUntil(() => taskCompleted == 40);
    }

    private async Task DemoTask(int id, object locker)
    {
        try
        {
            var threadId = Thread.CurrentThread.ManagedThreadId;

            using (await AsyncLock.LockAsync(locker))
            {
                _inOneThreadAndOneLockerInWorkCounter++;

                if (_inOneThreadAndOneLockerInWorkCounter != 1)
                    Assert.Fail("InOneThreadAndOneLockerInWorkCounter = {0}",
                        _inOneThreadAndOneLockerInWorkCounter);

                Debug.WriteLine("Int Current Id {0}, ThreadId = {1}", id, threadId);
                var waitTime = new Random((int) DateTime.Now.Ticks).Next(1, 100);
                await Task.Delay(waitTime).ConfigureAwait(false);
                Debug.WriteLine("Out Current Id {0}, ThreadId = {1}", id, threadId);

                _inOneThreadAndOneLockerInWorkCounter--;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        taskCompleted++;
    }

    [OneTimeTearDown]
    public void EndTest()
    {
        Trace.Flush();
    }

    private class LockClass
    {
    }
}