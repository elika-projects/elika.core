﻿using System;
using NUnit.Framework;
using System.Collections.Generic;

namespace Elika.Core.Tests
{
    public class BEBitConverterTests
    {
        private static int HalfSize = 10;

        private static IEnumerable<int> GetInt32Cases()
        {
            for (int i = 1; i <= HalfSize; i++)
                yield return int.MinValue / i;

            yield return 0;

            for (int i = 1; i <= HalfSize; i++)
                yield return int.MaxValue / i;
        }

        private static IEnumerable<uint> GetUInt32Cases()
        {
            yield return uint.MinValue;

            for (uint i = 1; i <= HalfSize * 2; i++)
                yield return uint.MaxValue / i;
        }

        private static IEnumerable<short> GetInt16Cases()
        {
            for (short i = 1; i <= HalfSize; i++)
                yield return (short)(short.MinValue / i);

            yield return 0;

            for (short i = 1; i <= HalfSize; i++)
                yield return (short)(short.MaxValue / i);
        }


        private static IEnumerable<ushort> GetUInt16Cases()
        {
            yield return ushort.MinValue;

            for (ushort i = 1; i <= HalfSize * 2; i++)
                yield return (ushort)(ushort.MaxValue / i);
        }

        [Test]
        [TestCaseSource(nameof(GetInt32Cases))]
        public void Int32Array(int expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToInt32(bytes);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetInt32Cases))]
        public void Int32ArraySegment(int expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToInt32(new ArraySegment<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetInt32Cases))]
        public void Int32Memory(int expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToInt32(new ReadOnlyMemory<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetUInt32Cases))]
        public void UInt32Array(uint expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToUInt32(bytes);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetUInt32Cases))]
        public void UInt32ArraySegment(uint expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToUInt32(new ArraySegment<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetUInt32Cases))]
        public void UInt32Memory(uint expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToUInt32(new ReadOnlyMemory<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetInt16Cases))]
        public void Int16(short expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToInt16(bytes);
            Assert.AreEqual(expected, actual);

            actual = BEBitConverter.ToInt16(new ArraySegment<byte>(bytes));
            Assert.AreEqual(expected, actual);

            actual = BEBitConverter.ToInt16(new Memory<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetInt16Cases))]
        public void Int16Array(short expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToInt16(bytes);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetInt16Cases))]
        public void Int16ArraySegment(short expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToInt16(new ArraySegment<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetInt16Cases))]
        public void Int16Memory(short expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToInt16(new ReadOnlyMemory<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetUInt16Cases))]
        public void UInt16Array(ushort expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToUInt16(bytes);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetUInt16Cases))]
        public void UInt16ArraySegment(ushort expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToUInt16(new ArraySegment<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(GetUInt16Cases))]
        public void UInt16Memory(ushort expected)
        {
            var bytes = BEBitConverter.GetBytes(expected);
            var actual = BEBitConverter.ToUInt16(new ReadOnlyMemory<byte>(bytes));
            Assert.AreEqual(expected, actual);
        }
    }
}
