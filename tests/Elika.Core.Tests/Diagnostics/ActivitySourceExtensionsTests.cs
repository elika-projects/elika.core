﻿using Elika.Diagnostics;
using NUnit.Framework;
using System.Diagnostics;

namespace Elika.Core.Tests.Diagnostics
{
    public class ActivitySourceExtensionsTests
    {
        [Test]
        public void StartActivityTest()
        {
            var activitySource = new ActivitySource("Elika.Core.Tests");
            var activityListener = new ActivityListener
            {
                ShouldListenTo = s => true,
                SampleUsingParentId = (ref ActivityCreationOptions<string> activityOptions) => ActivitySamplingResult.AllData,
                Sample = (ref ActivityCreationOptions<ActivityContext> activityOptions) => ActivitySamplingResult.AllData
            };
            ActivitySource.AddActivityListener(activityListener);

            var activity = activitySource.StartActivityWithFullName();
            Assert.IsNotNull(activity);
            Assert.AreEqual($"{nameof(ActivitySourceExtensionsTests)}.{nameof(StartActivityTest)}", activity?.OperationName);
        }
    }
}
