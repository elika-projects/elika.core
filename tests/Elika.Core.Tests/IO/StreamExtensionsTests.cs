﻿using System.IO;
using System.Threading.Tasks;
using Elika.IO;
using NUnit.Framework;

namespace Elika.Core.Tests.IO;

public class StreamExtensionsTests
{
    [Test]
    public void ReadWriteInt16Test()
    {
        const short expected = -12_345;
        const int expectedLength = 2;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadInt16();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public void ReadWriteUInt16Test()
    {
        const ushort expected = 12_345;
        const int expectedLength = 2;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadUInt16();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public void ReadWriteInt32Test()
    {
        const int expected = -12_345;
        const int expectedLength = 4;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadInt32();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public void ReadWriteUInt32Test()
    {
        const uint expected = 12_345;
        const int expectedLength = 4;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadUInt32();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public void ReadWriteInt64Test()
    {
        const long expected = -12_345;
        const int expectedLength = 8;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadInt64();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public void ReadWriteUInt64Test()
    {
        const ulong expected = 12_345;
        const int expectedLength = 8;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadUInt64();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public void ReadWriteFloatTest()
    {
        const float expected = -12_345;
        const int expectedLength = 4;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadSingle();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public void ReadWriteDoubleTest()
    {
        const double expected = -12_345;
        const int expectedLength = 8;
        using (var mem = new MemoryStream())
        {
            mem.Write(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = mem.ReadDouble();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteInt16AsyncTest()
    {
        const short expected = -12_345;
        const int expectedLength = 2;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadInt16Async();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteUInt16AsyncTest()
    {
        const ushort expected = 12_345;
        const int expectedLength = 2;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadUInt16Async();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteInt32AsyncTest()
    {
        const int expected = -12_345;
        const int expectedLength = 4;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadInt32Async();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteUInt32AsyncTest()
    {
        const uint expected = 12_345;
        const int expectedLength = 4;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadUInt32Async();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteInt64AsyncTest()
    {
        const long expected = -12_345;
        const int expectedLength = 8;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadInt64Async();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteUInt64AsyncTest()
    {
        const ulong expected = 12_345;
        const int expectedLength = 8;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadUInt64Async();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteFloatAsyncTest()
    {
        const float expected = -12_345;
        const int expectedLength = 4;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadSingleAsync();
            Assert.AreEqual(expected, res1);
        }
    }

    [Test]
    public async Task ReadWriteDoubleAsyncTest()
    {
        const double expected = -12_345;
        const int expectedLength = 8;
        using (var mem = new MemoryStream())
        {
            await mem.WriteAsync(expected);

            Assert.AreEqual(expectedLength, mem.Length);

            mem.Seek(0, SeekOrigin.Begin);

            var res1 = await mem.ReadDoubleAsync();
            Assert.AreEqual(expected, res1);
        }
    }
}