﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Elika.Linq.Expressions;
using NUnit.Framework;

namespace Elika.Core.Tests.Linq.Expressions;

public class ExpressionExtensionsTests
{
    [Test]
    public void GetPropertyNamesTest()
    {
        Expression<Func<List<int>, object>> expr1 = l => l.Count;
        AssertExpr(expr1, "Count");

        Expression<Func<List<int>, object>> expr2 = l => new {l.Count};
        AssertExpr(expr2, "Count");

        Expression<Func<List<int>, object>> expr3 = l => new {l.Count, l.Capacity};
        AssertExpr(expr3, "Count", "Capacity");
    }

    public void AssertExpr(Expression expr, params string[] expected)
    {
        var res = expr.GetPropertyNames();
        Assert.AreEqual(expected, res);
    }
}