﻿using System;
using NUnit.Framework;

namespace Elika.Core.Tests;

internal class OperationResultTests
{
    public static string Result = "OK";

    [Test]
    public void OperationResultReturnTest()
    {
        var result1 = OperationResultReturnMethod(1, 1);
        Assert.IsTrue(result1.IsSuccess);
        Assert.AreEqual(default(int), result1.Error);

        var result2 = OperationResultReturnMethod(2, 1);
        Assert.IsFalse(result2.IsSuccess);
        Assert.AreEqual(1, result2.Error);

        var result3 = OperationResultReturnMethod(1, 2);
        Assert.IsFalse(result3.IsSuccess);
        Assert.AreEqual(2, result3.Error);
    }

    public OperationResult<int> OperationResultReturnMethod(int a, int b)
    {
        if (a == b)
            return OperationResult<int>.SuccessResult;
        if (a > b)
            return 1;
        if (a < b)
            return 2;

        throw new ArgumentOutOfRangeException();
    }

    [Test]
    public void OperationResultReturnWithResultTest()
    {
        var result1 = OperationResultReturnWithResultMethod(1, 1);
        Assert.IsTrue(result1.IsSuccess);
        Assert.AreEqual(Result, result1.Result);
        Assert.AreEqual(default(int), result1.Error);

        var result2 = OperationResultReturnWithResultMethod(2, 1);
        Assert.IsFalse(result2.IsSuccess);
        Assert.AreEqual(default(string), result2.Result);
        Assert.AreEqual(1, result2.Error);

        var result3 = OperationResultReturnWithResultMethod(1, 2);
        Assert.IsFalse(result3.IsSuccess);
        Assert.AreEqual(default(string), result2.Result);
        Assert.AreEqual(2, result3.Error);
    }

    public OperationResult<string, int> OperationResultReturnWithResultMethod(int a, int b)
    {
        if (a == b)
            return Result;
        if (a > b)
            return 1;
        if (a < b)
            return 2;

        throw new ArgumentOutOfRangeException();
    }
}