﻿using Elika.Reflection;
using NUnit.Framework;

namespace Elika.Core.Tests.Reflection
{
    public class CallerTests
    {
        [Test]
        public void GetCallerMethodInfoTests()
        {
            var methodInfo = Caller.GetCallerMethodInfo();
            Assert.AreEqual(nameof(GetCallerMethodInfoTests), methodInfo?.Name);
        }
    }
}
