﻿using System.IO;
using NUnit.Framework;

namespace Elika.Core.Tests;

public class StreamTests
{
    [Test]
    public void Test()
    {
        var data = new byte[] {1, 2, 3};
        var mem = new MemoryStream(data);

        var target = new byte[4];

        var res = mem.Read(target, 0, 4);
    }
}