﻿using System;
using NUnit.Framework;

namespace Elika.Core.Tests;

public class TimeSpanExtensionsTests
{
    [Test]
    public void AddDays()
    {
        var timeSpan = new TimeSpan(1, 0, 0, 0);

        var expected1 = new TimeSpan(6, 0, 0, 0);
        Assert.AreEqual(expected1, timeSpan.AddDays(5));

        var expected2 = new TimeSpan(-4, 0, 0, 0);
        Assert.AreEqual(expected2, timeSpan.AddDays(-5));
    }

    [Test]
    public void AddHours()
    {
        var timeSpan = new TimeSpan(1, 0, 0, 0);

        var expected1 = new TimeSpan(1, 5, 0, 0);
        Assert.AreEqual(expected1, timeSpan.AddHours(5));

        var expected2 = new TimeSpan(0, 19, 0, 0);
        Assert.AreEqual(expected2, timeSpan.AddHours(-5));
    }

    [Test]
    public void AddMinutes()
    {
        var timeSpan = new TimeSpan(1, 0, 0, 0);

        var expected1 = new TimeSpan(1, 0, 5, 0);
        Assert.AreEqual(expected1, timeSpan.AddMinutes(5));

        var expected2 = new TimeSpan(0, 23, 55, 0);
        Assert.AreEqual(expected2, timeSpan.AddMinutes(-5));
    }

    [Test]
    public void AddSeconds()
    {
        var timeSpan = new TimeSpan(1, 0, 0, 0);

        var expected1 = new TimeSpan(1, 0, 0, 5);
        Assert.AreEqual(expected1, timeSpan.AddSeconds(5));

        var expected2 = new TimeSpan(0, 23, 59, 55);
        Assert.AreEqual(expected2, timeSpan.AddSeconds(-5));
    }
}