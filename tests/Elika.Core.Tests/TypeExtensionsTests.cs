﻿using System;
using Elika.Exceptions;
using NUnit.Framework;

namespace Elika.Core.Tests;

public class TypeExtensionsTests
{
    [Test]
    public void IsInterfaceDefinitionOfTest()
    {
        var definitionType = typeof(IInterface<>);
        Assert.IsTrue(typeof(ClassB1).IsInterfaceDefinitionOf(definitionType));
        Assert.IsTrue(typeof(ClassC1).IsInterfaceDefinitionOf(definitionType));
        Assert.IsFalse(typeof(ClassA1<>).IsInterfaceDefinitionOf(definitionType));

        Assert.Catch<InterfaceTypeRequiredException>(() =>
            typeof(ClassB1).IsInterfaceDefinitionOf(typeof(ClassA1<>)));
        Assert.Catch<NonDefinitionTypeException>(() =>
            typeof(ClassB1).IsInterfaceDefinitionOf(typeof(IInterface<string>)));
        Assert.Catch<ArgumentNullException>(() => typeof(ClassB1).IsInterfaceDefinitionOf(null));
        Assert.Catch<ArgumentNullException>(() => ((Type) null).IsInterfaceDefinitionOf(typeof(IInterface<>)));
    }

    [Test]
    public void IsClassDefinitionOfTest()
    {
        var definitionType = typeof(ClassA1<>);
        Assert.IsTrue(typeof(ClassB1).IsClassDefinitionOf(definitionType));
        Assert.IsTrue(typeof(ClassC1).IsClassDefinitionOf(definitionType));
        Assert.IsFalse(typeof(ClassC1).IsClassDefinitionOf(typeof(ClassA2<>)));

        Assert.Catch<ClassTypeRequiredException>(() => typeof(ClassB1).IsClassDefinitionOf(typeof(IInterface<>)));
        Assert.Catch<NonDefinitionTypeException>(() =>
            typeof(ClassB1).IsClassDefinitionOf(typeof(ClassA1<string>)));
        Assert.Catch<ArgumentNullException>(() => typeof(ClassB1).IsClassDefinitionOf(null));
        Assert.Catch<ArgumentNullException>(() => ((Type) null).IsClassDefinitionOf(typeof(ClassA1<>)));
    }

    [Test]
    public void ParentGenericDefinitionTest()
    {
        var definitionType = typeof(ClassA1<>);

        Assert.IsTrue(typeof(ClassB1).IsClassDefinitionOf(definitionType));
        Assert.IsTrue(typeof(ClassC1).IsClassDefinitionOf(definitionType));
        Assert.IsFalse(typeof(ClassC2).IsClassDefinitionOf(definitionType));
    }

    [Test]
    public void ParentGenericTest()
    {
        var genericTrue = typeof(ClassA1<object>);
        var genericFalse = typeof(ClassA1<int>);

        Assert.IsTrue(typeof(ClassC1).IsParentOf(genericTrue));
        Assert.IsFalse(typeof(ClassC1).IsParentOf(genericFalse));
    }

    [Test]
    public void ParentClassTest()
    {
        var parentType = typeof(ClassB1);

        Assert.IsTrue(typeof(ClassC1).IsParentOf(parentType));
    }

    [Test]
    public void ExceptionTests()
    {
        Assert.Catch<ArgumentNullException>(() => ((Type) null).IsParentOf(null));
        Assert.Catch<ArgumentNullException>(() => typeof(ClassC1).IsParentOf(null));
    }

    private class ClassA1<T>
    {
    }

    private class ClassA2<T>
    {
    }

    private class ClassB1 : ClassA1<object>, IInterface<string>
    {
    }

    private class ClassC1 : ClassB1
    {
    }

    private class ClassC2
    {
    }

    private interface IInterface<T>
    {
    }
}